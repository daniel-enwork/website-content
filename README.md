# Website Content
The enwork website content.
Edit this content to change the pages and the blog posts.

## Editing
Open this project in Visual Studio Code for an improved editing experience.

## Structure
### Page Configs
The `pages` directory contains alls config files for the pages of the website.
These configs should be `json` files and cannot be placed into subdirectories.

### Blog Posts
The `blog-posts` directory contains all blog posts.
These should be combinations of a Markdown file and a JSON file.
The Markdown file contains the text of the post.
The JSON file contains all information about the post.

# Validation
There are two tasks to run. 
`Validate Pages"` and `Validate Blog Posts`.
Issue them with `Shigt+Command+P` and type in `Run Task` and select one of the Tasks and run without scanning for errors.
Alternative you can select the top-level menu `Terminal` and select `Run Task` and select the task to run in the popup.

##