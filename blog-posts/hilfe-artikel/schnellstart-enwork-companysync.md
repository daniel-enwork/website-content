Mit dem enwork CompanySync, unserer JavaScript API, bieten wir dir eine schnelle und einfache Möglichkeit,
deine Stellendaten von enwork abzurufen und für deine eigene Webseite zu verwenden.
Dafür stellen wir dir einen JavaScipt Client bereit, mit dem Du Lese-Zugriffe auf
unsere API hast und so die Daten deines Unternehmens einsehen kannst. Die daraus
resultierenden Rohdaten kannst Du selbst verarbeiten und völlig frei auf deiner
Webseite darstellen.  

---
*Was ist eine API?*  
Eine **API** (von application programming interface) ist die *Schnittstelle*, an
der sich zwei System austauschen können. Sie ermöglicht dir, deine Daten von
unserer Webseite auf deine eigene zu übertragen.

---

### Einbinden und Initialisieren des JavaScript Client
Um den JavaScript Client nutzen zu können, musst Du ihn mithilfe eines Script-Tags
auf deiner Seite einbinden. Weiterhin benötigst Du einen API Token, damit Du dich
gegen die API authentifizieren kannst. Beides findest Du in deinem Unternehmens-Profil
im Tab Verknüpfung (Klicke auf der Startseite oben rechts auf deinen Unternehmensnamen,
jetzt siehst Du den Tab *Verknüpfung* oben in der Tab-Leiste).

---
*Was sind Script-Tags und API Token?*  
Mit **Script-Tags** lädst Du Code von uns herunter.
Die sagen dann deiner Webseite, was sie zu machen hat.  
Ein **API Token** kannst Du dir wie ein Passwort vorstellen, damit wir wissen, auf
welche Daten Du zugreifen darfst.

---

### Beispiel JavaScript-Tag:
Das ist der Script-Tag (auch **Snippet** genannt):  
```
<script src="https://static.enwork.de/js-sdk/0.0.0/enwork-sdk.min.js"></script>
```

Diesen Script-Tag musst Du im Head-Bereich des HTML deiner Webseite einbinden. Wir
stellen dir dann unseren Client in der globalen Variable *enwork* zur Verfügung,
sobald dass Skript geladen wurde. 

Mit dem Befehl **init()** startest Du den Client und hinterlegst dein API-Token.
Jetzt kannst Du mit weiteren Befehlen alle deine Daten abrufen.

#### Beispiel-Daten zum Ausprobieren
Wenn dein Unternehmen noch kein enwork Profil besitzt oder wenn Du noch keine
Stellenanzeigen eingestellt hast, kannst Du dich gerne an unseren
Beispiel-Daten ausprobieren. Die bekommst Du ganz einfach, indem Du das
API-Token *TEAMENWORK* benutzt.

---
_Der **Befehl** zum starten: enwork.init(DEIN_API_KEY)_  

*Wo finde ich den Head-Bereich und was ist die globale Variable?*  
Der **Head Bereich** findet sich für jede Webseite in einer Konsole. Die lässt sich per
Rechtsclick und *inspect* aufrufen.  
**Globale Variable** heißt nichts anderes, als dass ein Programmierer von *überall Zugriff*
auf den Client hat.

---
### Deine Daten abrufen
Sobald Du den Client aktiviert hast, stehen dir deine Daten zur Verfügung. Für eine
grobe Übersicht kannst Du eine Liste deiner Stellenanzeigen bei uns abrufen. Soll
es eine detaillierte Ansicht sein, kannst Du auch alle Informationen einer
bestimmten Stelle abrufen.

#### Abrufen einer Liste aller Stellen
Um eine Liste deiner Stellenanzeigen abzurufen, stellen wir dir den Befehl 
**getVacancyList()** bereit. Er liefert dir eine Übersicht an Daten zu deinen Stellen,
die Du jetzt übersichtlich als Liste darstellen kannst. 

#### Abrufen aller Informationen zu einer Stelle
Natürlich kannst Du auch vollständige Stellenanzeigen auf deiner Webseite darstellen.
Dafür stellen wir dir den Befehl **getVacancyDetails(VACANCY_ID)** bereit. Der ermöglicht
dir den Abruf der gesamten Daten einer Stelle. Dafür benötigst Du lediglich die ID
einer bestimmten Stellenanzeige. Die ID erhälst Du zum Beispiel durch den Listen-Aufruf. 

---
_Der **Befehl** für eine Liste deiner Stellenanzeigen: enwork.getVacancyList()_  
_Der **Befehl** für vollständige Stellenanzeigen: enwork.getVacancyDetails(VACANCY_ID)_

Diese zwei Aufrufe liefern dir alle notwendigen Daten, um z.B. ein ganzes Job-Board
auf deiner eigenen Webseite zu integrieren. So erreichst Du noch mehr potenzielle Bewerber
für deine offenen Stellen. 

---

### Weitere Informationen
Neben den oben erwähnten Befehlen gibt es noch weitere Möglichkeiten, Daten von enwork
abzurufen. Zum Beispiel kannst Du auch auf sämtliche Daten aus deinem Unternehmensprofil
zugreifen. Eine Übersicht über die Befehle, die dir zur Verfügung stehen, und eine
detaillierte Aufgliederung deiner Daten findest Du in unserer [technischen Dokumentation][].

Um dir ein besseres Bild machen zu können, haben wir dir außerdem ein komplett
funktionsfähiges Beispiel gebaut, das Du in unserem [Git-Repository][] findest.
Du kannst Teile des Codes daraus gerne verwenden, um deine eigene Integration
schneller an den Start zu bekommen. 

### Du hast noch Fragen?
Kein Problem! Wir stehen dir bei der Integration natürlich zur Seite. Falls Du
gar nicht weiter weißt, wende dich gerne an unseren [Support][].
Außerdem haben wir ein kleines Q&A für dich vorbereitet. 
 
#### Da gibt es doch diese DSGVO, was muss ich tun?
Wir haben unser Snippet so datenschutzfreundlich wie möglich gestaltet. Das bedeutet,
wir erstellen keine Verhaltensprofile deiner Nutzer und erfassen auch sonst keine
Tracking-Daten auf deiner Webseite. Da das Snippet jedoch von unseren Servern geladen
wird und die Stellendaten von unseren Servern abfragt, bauen die Browser deiner Besucher
eine Verbindung mit uns auf. In diesem Zuge erfassen unsere Server in ihren Protokollen
die üblichen Informationen über Browsertyp und Browserversion, verwendetes Betriebssystem,
Referrer URL und Hostname des zugreifenden Rechners sowie die Uhrzeit der Serveranfrage
und die IP-Adresse.
Da wir diese Protokoll-Daten erfassen und speichern, musst Du deine Besucher auf diesen
Umstand **in deiner Datenschutzerklärung hinweisen**. Damit Du es so einfach wie möglich
hast, stellen wir dir eine Text-Vorlage bereit, die Du in deine Datenschutzerklärung
übernehmen kannst. Die Vorlage findest Du [hier][].  

Bitte beachte aber, dass wir für diesen Text und dessen Rechtssicherheit keine Haftung
übernehmen können und Du solche Sachverhalte immer vorher mit deinem Datenschutzbeauftragten
besprechen solltest. 

#### Meine Webseite nutzt eine Content-Security-Policy, was muss ich freischalten?
Wenn deine Webseite eine Content-Security-Policy einsetzt, musst Du folgende
Hostnames auf die sogenannte *Whitelist* setzen, damit Du unser Skript und die Bilder
aus unseren Datensätzen nutzen kannst.

Source | Hostname
--- | ---
script-src | static.enwork.de
script-src-elem | api.enwork.de
image-src | storage.googleapis.com

#### Kann ich den Client auch ohne Vorkenntnisse in JavaScipt nutzen?
Nein, ohne Vorwissen in JavaScript kannst Du den Client aktuell nicht nutzen. Wir
arbeiten aber an einer Version, mit der Du auch ohne spezielle Kenntnisse das Aussehen
der Stellenanzeigen verändern kannst.  
Alternativ kannst Du ein sogenanntes *iframe* in deine Seite einbetten, dazu brauchst Du
auch keine Vorkenntnisse. Wir rendern dann die Seite für dich, Du kannst die Darstellung der
Stellenanzeigen ohne HTML- oder JavaScript-Kenntnisse aber nicht anpassen. Stell' dir
das iframe als Fenster vor, durch das Du auf unsere Webseite schaust.  
Du kannst dich wie bei allen Fragen auch hier gerne an unseren [Support][] wenden.

#### Kann ich auch Daten von meiner Webseite in mein enwork Profil übertragen?
Nein, enwork ist als Hub konzipiert. Das bedeutet, dass alle Daten zentral über
enwork laufen und von dort an alle deine weiteren Services (wie zum Beispiel die JavaScript API
und deine Webseite) weitergeleitet werden. Da das nur in eine Richtung fuktioniert, kannst Du deine
Daten nicht über einen von dir definierten Service (wie z.B. deine eigene Webseite) in dein enwork-Profil integrieren. Kurzum: Du kannst deine Services und Systeme mit Daten von enwork speisen, aber enwork nicht mit Daten aus deinen Services und Systemen.  
Solltest du Probleme bei der Erstellung deines Profil haben, zögere nicht unseren
[Support][] um Hilfe zu bitten.

[technischen Dokumentation]: https://static.enwork.de/js-sdk/0.0.0/docs/globals.html
[Git-Repository]: https://gitlab.com/enwork/enwork-example-integration
[hier]: https://static.enwork.de/legal-documents/company-sync-privacy-note/company-sync-privacy-note-de.pdf
[Support]: https://www.enwork.de/support
