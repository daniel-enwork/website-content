Die Elternzeit war erfüllend und so langsam müssen die Kleinen auch nicht mehr
ständig beaufsichtigt werden. Mit diesem guten Gefühl in der Brust zieht es dich
wieder zurück ins Berufsleben – aber wie glückt der Wiedereinstieg nach der
Auszeit? Und was ist überhaupt möglich? Schließlich sind die Kinder trotz allem
noch nicht selbstständig. Da ist es ganz normal, wenn auch Unsicherheit aufkommt.  
Die rechte Balance zu finden erfordert viel Feingefühl.

### Denkt denn einer an die Kinder?
Der Sprung aus der Elternzeit zurück in den Beruf ist mit Hürden verbunden. Es
ist ja so schon kompliziert genug, die passende Stelle zu finden. Mit den Kindern im
Hinterkopf wird es aber noch ein wenig anspruchsvoller. Die Stelle könnte dich zu
sehr beanspruchen und nicht ausreichend Zeit für den Nachwuchs übrig lassen.
Außerdem solltest Du beachten, dass der Job dich nicht zu sehr in deiner
Flexibilität einschränkt. Mit Kindern zuhause kann es schwierig werden, wenn Du
dir deine Arbeitszeit nicht einteilen kannst. Es könnte auch Probleme geben, wenn
Du gar mal etwas spontan umstellen möchtest. Neben den praktischen Aspekten
könnte auch deine Konzentrationsfähigkeit darunter leiden, wenn Du diese Faktoren
nicht zu deiner Zufriendheit klären kannst. Alles in allem kann so eine
Belastung entstehen, von der weder Du noch deine Kinder profitieren.

### Neuer Job, neues Glück
Und doch birgt der Wiedereinstieg großes Potenzial für dich. Es geht nicht nur
darum, wieder eine Einkommensquelle zu finden. Viel mehr beginnt eine neue
Lebensphase für dich. Eine Chance, dich neu auszurichten und weiterzuentwickeln.
Wenn Du vorher nichts neues wagen wolltest, weil Du dich so gut eingerichtet
hattest und alles rund lief, kannst Du jetzt die Gelegenheit beim Schopfe packen.
Welche Stelle hat dich schon immer interessiert? Vielleicht sehnst Du dich ja nach
einem frischen Aufgabenbereich, um neue Erfahrungen zu sammeln. Letztendlich
könnte es auch Zeit für einen Tapetenwechsel sein. Du kannst den Wiedereinstieg
nach der Elternzeit nämlich auch mit einem Quereinstieg in einen gänzlich anderen
Beruf verbinden. Dein Berufsweg ist noch lange nicht zu Ende und dementsprechend
auch nicht deine Möglichkeiten, dich selbst zu entdecken und zu entfalten.  

### Die passende Stelle
Für einen gelungenen Wiedereinstieg braucht es also eine Stelle, die zu dir passt.
Die zu finden kann aber ähnlich nervenaufreibend sein wie die Stimmungsschwankungen
eines Kleinkinds. Die Jobsuche geht oft immer noch mit dem Lesen vieler
undurchsichtig geschriebener Jobausschreibungen einher und lustigem Rätselraten,
mit welchem zeitlichen Rahmen Du im neuen Job rechnen musst. Du kannst dir einen
Großteil der Arbeit auch sparen. Bei enwork kannst Du schon während der
Profilerstellung vermerken, welches Arbeitszeitmodell dir wichtig ist und wie
flexibel Du sein möchtest. Wir vergleichen deine Angaben dann mit unserem Pool
an Jobangeboten und schlagen dir die Stellen vor, die am besten zu dir passen.
So musst Du nicht lange Zeit mit Suchen verbringen, sondern bekommst interessante
Stellen von uns direkt serviert. Zeit, die ohnehin besser in den Nachwuchs
investiert ist.  
Leg' jetzt [dein Profil](https://app.enwork.de/sign-up) an und steig' erfolgreich
wieder ins Berufsleben ein.
