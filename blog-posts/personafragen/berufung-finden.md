Der Job bringt genug ein und Du stehst sicher im Leben. Privat läuft alles
ganz okay soweit aber Du merkst, dass dir etwas fehlt. Die Familie ist zufrieden
und erfüllt dich, mit deinen Freunden hast Du Spaß und wenn Du deinem Hobby
nachgehst, gehst Du auf. Du möchtest am liebsten noch mehr Interessen verfolgen
aber dafür reicht die Zeit einfach nicht, die Arbeit macht sich schließlich nicht
von alleine. Dabei ist dein Job mehr Mittel zum Zweck, das Du billigend in Kauf
nimmst. So richtig glücklich macht er dich aber nicht.

### Kein guter Deal
Den größten Anteil deiner Zeit opferst Du deinem Job, damit Du in der
verbleibenden Zeit das machst, was dir Freude bereitet. Klar, irgendwie muss das
Essen auf den Tisch kommen und die Miete wird auch nicht weniger. Wenn Du nun aber
deine Zeit und Energie im grauen Arbeitsalltag aufwendest, nur um den Status quo
zu halten, reißt es auch die frische Luft in deiner Freizeit nicht mehr raus.
Auf Dauer stellt sich Unzufriedenheit ein. Üblicherweise geht damit das Gefühl
einher, seine Zeit zu verschwenden und etwas zu verpassen. Aus dem Konzept
*Arbeiten um zu Leben* wird bevor man's sich versieht *Leben um zu Arbeiten*.
An diesem Punkt angekommen kann die Frage aufkommen, was Du eigentlich mit deiner
Zeit machst und ob da nicht mehr für dich drin wäre. Keine schwere Frage.


### Der Ruf deiner Leidenschaft
Wahrscheinlich hast Du jetzt schon ein paar Ideen, was Du lieber machen würdest,
als dich wieder an die Arbeit zu setzen. Tatsächlich geht aber nicht jeder
widerwillig zur Arbeit. Einige glückliche Seelen fühlen sich regelrecht *berufen*
wenn sie sich morgens auf die anstehenden Aufgaben vorbereiten. Was machen die
anders als Du?  
Sie haben den Ruf gehört und folgen ihren Interessen und Leidenschaften. Der Job
kann so viel mehr sein als ein notwendiges Übel, um Geld zu verdienen. So wie Du
in deinem Hobby aufgehst, kannst Du dich auch im Job entfalten. Es gibt so viele
Berufe, in denen deine Interessen zum Tragen kommen und in denen Du dich
weiterentwickeln kannst. Wenn Du die nächste Stelle anhand deiner Persönlichkeit
und Leidenschaft auswählst und nicht am Gehalt festmachst, wird dein Beruf zur
Berufung.

### Die Welt steht dir offen
Es gibt unzählige Jobs und Arbeitsfelder da draußen. Gut möglich, dass Du von
dem Beruf, der zu dir passt, noch nie etwas gehört hast. Bevor Du jetzt also mit
einem Kopfsprung in dieses Meer aus Möglichkeiten springst, solltest Du dir zuerst
Gedanken über dich selbst machen. Was suchst Du in deiner Berufung, was willst Du
machen und wie willst Du arbeiten? Du kannst all diese Fragen auf enwork
beantworten, indem Du dein Profil anlegst. Gib an, was dir wichtig ist und was
dich interessiert. Wir gleichen deine Vorlieben mit unserem Pool von Jobangeboten
ab und schlagen dir die passendsten Stellen vor. So findest Du die idealen Jobs
ohne lange suchen zu müssen. Außerdem können wir dir Berufsvorschläge machen,
auf die Du selbst vielleicht nie gekommen wärst.  
Erstelle jetzt [dein Profil](https://app.enwork.de/sign-up) und mach deinen Beruf
zu Berufung.
