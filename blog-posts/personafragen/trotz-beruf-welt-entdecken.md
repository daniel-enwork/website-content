Wer viel arbeitet, bewegt auch viel. Aber wer viel arbeitet hat eben auch kaum
Freizeit. Oft sind die Aufgaben im Job auch nicht gerade so erfüllend, das man
guten Gewissens den Großteil seiner Zeit opfert. Die Möglichkeit, neben der Arbeit
etwas von der Welt zu sehen, beläuft sich dabei auf doch eher überschaubare
Urlaubstage. So verabschiedet sich die Hoffnung, den Horizont mit
eindrucksvollen Erfahrungen zu erweitern. Wenn die Work-Life-Balance in Schieflage
gerät, fallen viele einmaligen Momente hinten runter.

### Leben um zu arbeiten?
Es passiert schneller als man denkt. Man vertieft sich immer mehr in die Arbeit,
nimmt mehr und mehr Aufgaben an und fast unmerklich hat sich der Job einen
unverhältnismäßig großen Anteil am Leben erschlichen. Du hast weniger Zeit für
deine Hobbys und die Menschen, die dir wichtig sind. Bald schon fällt dir auf,
dass Du Gelegenheit um Gelegenheit ausschlagen musst, weil Du entweder keine Zeit
oder keine Energie hast. So gehen dir einmalige Erfahrungen verloren. Einige, die
Du aufzählen kannst und solche, von denen Du nie wissen wirst, dass Du sie verpasst
hast. Vor allem Reisen sind von dieser Arbeitsweise bedroht. Da deine ganze Energie
im Job steckt, wird es schwer, alle möglichen Länder und fremde Kulturen zu erkunden.
Wenn Du aber ein Auge auf deine Work-Life-Balance hast, verpasst Du nichts vom Leben.

### Du setzt deine Prioritäten
Du trägst die Verantwortung, deine Arbeit mit deinem Leben zu vereinbaren.
Arbeitgeber kommen dir bei dieser Aufgabe mal mehr mal weniger entgegen. Ob eine
Stelle wirklich zu dir passt, hängt eng damit zusammen, wie Du damit zurecht kommst.
Es kann sich lohnen, über einen Wechsel nachzudenken, wenn Du dich von der Arbeit
erdrückt fühlst. Viele Arbeitgeber bieten dir die Möglichkeit, deine
Arbeitsweise passend zu formen. Oft kannst Du über Home Office auch von zuhause
arbeiten. Wenn Du mal eine längere Auszeit brauchst, kannst Du bei einigen Jobs
ein sogenanntes *sabbatical* einlegen. Wenn Du schon eine Weile, oft drei Jahre,
in einem Unternehmen angestellt bist, bieten dir einige Arbeitgeber einen
verlängerten Urlaub von bis zu einem Jahr.


### Finde dein Gleichgewicht
Bisher findest Du genauso viele Jobs mit als auch ohne den Freiheiten, die dir
passen. Nun musst Du das bisschen Freizeit, das dir bleibt, nicht mit der langen
Jobsuche und anstrengenden Bewerbungen verschwenden. Du kannst dir auf enwork dein
individuelles Persönlichkeitsprofil anlegen. Unser Jobmatching berücksichtigt alle
deine Interessen und Prioritäten und vergleicht sie mit verschiedensten
Arbeitgebern. So können wir dir die Jobs vorschlagen, die genau zu deinem
Lebensentwurf passen. So behälst Du Zeit für dich und findest trotzdem einen Job,
der dir mehr Freiheit und eine bessere Work-Life-Balance bietet. Du verpasst nichts
von deinem Leben und kannst gleichzeitig deinem Beruf nachgehen.  
Erstelle jetzt [dein Profil](https://app.enwork.de/sign-up) und entdecke die Welt
neben dem Beruf.
