Langsam wird's ernst. Der Abschluss als letztes vorgegebenes Ziel, danach musst
Du dir selbst Gedanken machen. Wo soll's hingehen? Nicht in den Hörsaal – einem
next Level Klassenraum, wenn man mal ehrlich ist. Es soll praxisnahes Arbeiten
werden, so viel steht schon mal fest. Da bietet sich eine Ausbildung natürlich
an, aber welche? Dir öffnet sich ein buntes Spektrum an Berufsfeldern mit
verschiedenen Karrierewegen und praktischem Wissen in allen möglichen Bereichen.
Klar, dass da was für dich dabei ist. Aber auch genug, das nichts für dich ist.

### Der Schritt ins Berufsleben
Die Auswahl kann am Anfang überwältigend wirken. Mit der Ausbildung legst Du dich
für die kommenden drei Jahre und idealerweise auch darüber hinaus auf einen
Berufsweg fest. Die Wahl will also wohl überlegt sein. Du willst ja den ersten
Schritt richtig machen und möglichst gut ins Berufsleben starten.  
Wenn Du nicht die passende Wahl triffst, merkst Du das meist schon im ersten
Lehrjahr. So schlimm wird es schon nicht sein und man soll ja nicht immer gleich
aufgeben wenn's, schwierig wird. Doch je mehr Zeit vergeht, desto deutlicher siehst
Du es. Der Beruf ist nicht gerade so, wie Du ihn dir vorgestellt hast. Das, worauf
Du dich gefreut hast, kommt unerwartet kurz und die Arbeitsweise entspricht dir
auch nicht so richtig. Zwar lässt sich das eine Weile ertragen aber für viele
kommt irgndwann der Punkt, an dem sie sinnvollerweise die Ausbildung abbrechen.

### Es beginnt mit dir
Wenn der Beruf nicht passt ist es meist besser, dich umzuorientieren.
Die Ausbildung abzubrechen ist trotzdem immer ärgerlich und wirft dich zurück.
Es macht also Sinn, vor dem Arbeitsbeginn herauszufinden, welche Berufe wirklich
zu dir passen. Jetzt loszugehen und alle Berufe auf diesem Planeten zu analysieren
ist nicht die effizienteste Herangehensweise. Du kommst schneller zu Ergebnissen,
wenn Du bei dir anfängst. Was willst Du von deinem Job? Wo liegen deine echten
Interessen, was kannst Du gut und was macht dir Spaß? Die Antworten auf diese
Fragen können dir als Schablone bei der Suche nach der passenden Ausbildung dienen.
Du und deine Persönlichkeit sind die Grundlagen für den Weg in die Berufswelt.
In der Schule wirst Du gemerkt haben, wo es läuft und was einfach nicht deins ist.
Jetzt liegt es an dir, dein Leben entsprechend auszurichten.

### Finde, was zu dir passt
Wenn Du weißt, wonach Du suchst, hast Du schon mal viel gewonnen. Die Suche selbst
kann aber immer noch langwierig und anstrengend sein. Viele Stellenanzeigen machen
nicht klar, was Du von der Ausbildung zu erwarten hast. Oft liest Du dir eine
Wall of Text durch und fühlst dich, als hättest Du einen Absatz von Goethe gelesen.
Und dann kommt erst noch die Bewerbung selbst. Du stehst aber nicht ganz alleine
da. Bei enwork unterstützen wir dich während des gesamten Bewerbungsprozess.
Angefangen bei der Profilerstellung: hier kannst Du deine Interessen, Fähikeiten
und Vorlieben angeben und füllst so gleichzeitig deinen Lebenslauf aus. Mit
unserem Jobmatching Tool gleichen wir dein Profil mit den Stellen in unserem Pool
ab und schlagen dir die passendsten Ausbildungen vor. So umgehst Du die lästige
Suche und kommst gleich an ideale Angebote.  
Leg' jetzt [dein Profil](https://app.enwork.de/sign-up) an und finde die Ausbildung,
die zu dir passt.
