Dein Bewerbungsschreiben ist dein Plädoyer für deinen Traumjob. Dabei geht es
nicht darum, die Personalabteilung um die Stelle zu bitten. Du willst viel mehr
die Augen der Leser für das öffnen, was Du schon längst erkannt hast. Und zwar
dass Du wie für die Stelle geschaffen bist.
Überzeugend wirkt dabei ein Bild von dir als Person. Mit Argumenten über deinen
Lebenslauf hinaus wird deutlich, was Du zu bieten hast. Zeig' dem Personaler,
warum deine [Kompetenzen][] und Qualifikationen – vor allem aber deine **Motivation**
und deine Ziele – genau das sind, was das Unternehmen sucht.

### Was kommt in mein Bewerbungsschreiben?
Die große Frage nach dem Inhalt. Im Grunde ist es kein Hexenwerk. Den roten Faden
spinnst Du, indem Du dich mit diesen Aspekten beschäftigst:

- Was macht dich in der Arbeitswelt aus?
- Was macht das Unternehmen und die ausgeschriebene Stelle aus?
- Warum sprechen die Antworten dafür, dass Du eingestellt werden solltest?

Zunächst einmal zu dir. Die Wahrheit ist vielleicht nicht hübsch aber immerhin
hilft sie dir beim Bewerbungsschreiben: Du bist nur insofern für den Personaler
interessant, als Du sein oder ihr Problem löst: die offene Stelle. Das heißt für dich,
dass Du dich mit deinen Fähigkeiten und deinen Erwartungen an deine Karriere
auseinandersetzt. Was kannst Du besonders gut und welche Aufgaben bereiten dir
besonders viel Freude? Welche Arbeitsweise liegt dir oder ist dir vielleicht sogar
wichtig? Für dein Anschreiben ist es sinnvoll, nicht einfach deinen Lebenslauf
auszuformulieren. Denk an deine [Soft Skills][], wie bist Du zwischenmenschlich aufgestellt?
Auch das, was Du noch nicht so gut kannst, ist ein Wegweiser. Was möchtest Du
noch lernen? Welches Potenzial siehst Du selbst in dir und wie willst Du es verwirklichen?  

![Affe schaut in Autospiegel][Spiegel]

Das bringt uns auch schon zu deinen Zielen und deiner **Motivation**.
Was treibt dich an, was willst Du erreichen? Nicht nur für die eine Stelle,
sondern für deine gesamte Berufsbahn hilft es zu wissen, wo Du hin willst. Wenn
Du dir eine konkrete **Mission** formulierst, findest Du auch die Jobs, die dich
persönlich weiterbringen.  
Was das beim Anschreiben bringt? Ganz einfach. Stimmen deine **Arbeitsweise** und
deine **Mission** mit denen des Unternehmens überein, hast Du einen großen
Vorteil. Wenn Du für das brennst, was Du machst, bist Du auch produktiver als deine
unmotivierten Kollegen. Ein Experte von [Udacity][] hat es auf den Punkt gebracht:
"Es kann den Unterschied machen, für den Job geeignet zu sein oder die
Stelle wirklich zu bekommen".

Du hast zwei Anlaufpunkte, um das  Unternehmen und die Stelle besser kennenzulernen.  

1. Die Stellenanzeige
2. Die Homepage des Unternehmens

In der Stellenanzeige liest Du, welche **Fähigkeiten** Du mitbringen musst und was sonst
noch für die Stelle wichtig ist. Auf der Unternehmenshomepage findest Du einige
Anhaltspunkte darüber, wie dein Wunschunternehmen gestrickt ist. Mit etwas Glück steht
sogar in der Stellenausschreibung, mit welcher **Unternehmenskultur** Du rechnen kannst.
Vergleiche deine Stärken und Vorlieben mit dem Unternehmen, bei dem Du dich bewirbst.
Wie wird dich die neue Stelle fordern? Was kannst Du leisten? Warum bist Du für die
Stelle geeignet? Wenn deine **Mission** zum Unternehmen passt, hast Du ein starkes
Argument in der Hand. Deine individuelle Motivation ist dein größter Trumpf.
Insbesondere wenn dein Lebenslauf noch nicht viel mehr aufweist, als deinen
Abschluss und einen Minijob als Kassierer. Solltest Du aber schon Erfahrung mitbringen,
kannst Du dem Personaler gerne von deinen beruflichen Erfolgen berichten.

### Wie ist mein Anschreiben aufgebaut?
![Mann schaut auf Tafel voller Dokumente][Aufbau]

Dein Bewerbungsschreiben stützt sich auf seine Struktur. Was Du letztendlich schreibst,
steht dabei natürlich im Vordergrund. Mit dem **passenden Rahmen** kannst Du das
meiste aus dem Inhalt herausholen. Die grundlegenden Bausteine deines Anschreibens sind:   

- die Kopfzeile
- der Hauptteil
- die Grußformel samt Anlagenverzeichnis

Die **Kopfzeile** enthält klassischerweise deine Kontaktdaten und die Daten deines Wunscharbeitgebers.
Wichtig ist dabei der volle Firmennamen, die Adresse und der Name deines Ansprechpartners.
Es lohnt sich wirklich herauszufinden, wer für Personalfragen zuständig ist.
Das Datum hat oben rechts in der Kopfzeile seinen Platz. Ein aktuelles Datum ist dabei
das erste Zeichen für den Personaler, dass er oder sie ein individuelles Anschreiben vor sich hat.
Der Eindruck verstärkt sich, wenn Du ihn oder sie in der Anrede persönlich ansprichst.

Im **Hauptteil** liegt wenig überraschend der Kern deines Bewerbungsschreibens. Hier
überzeugst Du den neuen Arbeitgeber oder eben nicht. Nun hast Du aber nicht
viel Platz. Für einen kurzen und zugleich wirksamen Hauptteil bietet sich die
sogenannte *5 Schritt Struktur* an. Sie führt den Leser in ein Thema ein und gibt
drei zwingende Argumente. Am Schluss steht die Aussage, von der Du den Leser überzeugen möchtest.

![Stift auf Papier mit den Worten 5 Schritte zum Ziel][Schritte]

Im Bewerbungsanschreiben ist das natürlich, dass Du ideal für die Stelle geeignet bist.
Den Grundstein für diesen Eindruck legt die Einleitung, in deinem Fall der erste Satz.
Wenn der sitzt, weckst Du die Aufmerksamkeit des Personalers. Sag' **deutlich** worum es geht.
Für welche Stelle bewirbst Du dich? Wenn der Name des Unternehmens fällt, fühlt
sich der Personaler angesprochen. Das macht es deinen inhaltlichen Argumenten leichter.
Das **stärkste** Argument entfaltet an **erster Stelle** die größte Wirkung. Mit den zwei
weiteren Argumenten kannst Du Feinheiten nachjustieren und das Gesamtbild von dir
festigen. Am Ende sagst Du noch einmal klar und deutlich, dass Du ins Unternehmen passt und
die richtige Wahl für die Stelle bist. Schließe deine Argumentation so **selbstbewusst**,
wie Du sie begonnen hast. So rundest Du den Hauptteil deines Anschreibens eindrucksvoll ab.

Die **Grußformel** ist, wie der Name vermuten lässt, nur noch Formsache. Mit freundlichen
Grüßen kannst Du nichts falsch machen, Höflichkeit hat sich noch immer ausgezahlt.
Lass dich auch bei deinen Grüßen ruhig von deinem [eigenen Stil](#Stil) inspirieren.
Deine handschriftliche Unterschrift verleiht deiner Bewerung in jedem Fall einen
persönlichen Touch.  
Letztlich macht ein Anlagenverzeichnis Personalerleben leichter.
Hier führst Du alle wichtigen Dokumente wie Zeugnisse, Zertifikate oder Nachweise
über Ehrenämter und dergleichen auf. Orientiere dich bei der Reihenfolge an deinem
Lebenslauf, das wirkt einheitlich.  
Dein Anschreiben sollte insgesamt nicht länger als **eine DinA4** Seite sein. Alles was darüber
hinausgeht wird in aller Regel schlicht und ergreifend nicht gelesen.


### Wie formuliere ich mein Bewerbungsschreiben?
![Brille und Lupe liegen auf Buch][Lupe]

Wie Du dein Bewerbungsschreiben ausformulierst, liegt bei dir. Natürlich gibt es
ein paar Tricks, die deinen Text leserlich und ansprechend gestalten. Mit diesen
Grundlagen im Hinterkopf kannst Du deinen **eigenen Stil** ideal in Szene setzen.
Zunächst wirkt die berüchtigte *Wall of Text* erschlagend. Kurze Absätze hingegen
lassen sich schnell aufnehmen und strengen den Leser nicht an. Eine einheitlich
Gestaltung trägt dazu bei, dass deine Bewerbung wie ein zusammenhängendes Werk
wahrgenommen wird. Bleib deinen Design Entscheidungen wie etwa der Schiftart und Farbe
treu – und zwar im Anschreiben wie auch in deinem [Lebenslauf][]. Sogenannte *Serifenschriften*
wie Times New Roman oder Georgia sind übrigens besonders leicht zu lesen.  

<a name="Stil"></a>
Ob Du dir eine Vorlage aus dem Netz ziehst oder deine eigenen Gedanken zur Struktur
gemacht hast, an deinen eigenen Formulierungen kommst Du nicht vorbei. Das heißt nicht,
dass Du Schriftsteller werden sollst. Kreativ sein ist einfacher als Du vielleicht vermutest.
Dabei hilft es, wenn Du deinem Text einen **Funktion** gibst. Was soll der Text erreichen?
Dein Bewerbungsschreiben bringt dich dem neuen Job einen Schritt näher, wenn der Leser
**Interesse** für dich entwickelt. Das Interesse weckst Du mit Ausdrücken, die der Personaler
nicht auch schon in 20 anderen Bewerbungen gelesen hat. Abgegriffene Floskeln können dich
als Person nicht abbilden. **Emotionen** hingegen machen greifbar, warum Du zur Stelle passt.
Dahinter steckt ein Konzept aus der Psycholinguistik. Je stärker ein Text die grundlegenden
Aspekte von Gefühlen aktiviert, desto interessierter der Leser.  

![Schlüsselbund liegt auf Tisch][Schlüssel]

Der Schlüssel dafür sind **Metaphern**. Du musst gar nicht groß von deinen innersten
Gefühlen schwärmen, noch musst Du dir die ausgefallensten Metaphern aus der Nase ziehen.
Wenn Du mal auf alltägliche Ausdrücke achtest, fällt dir blitzschnell auf, wie häufig
wir in Bildern sprechen. Bilder wie *ich brenne für Innovation* oder *ich blühe
in Teamarbeit auf* sagen so viel mehr, als dass Du etwas gerne machst. Verschiedene
Studien zeigen, dass solche Metaphern im Leser Emotionen auslösen. Das kannst Du dir
in deinem Anschreiben zu Nutze machen. Baue die passenden Metaphern in deinen Text ein,
um einen soliden Eindruck zu hinterlassen.  

Besonders im ersten Satz geht es darum, **Aufmerksamkeit** zu erzeugen. Selbstbewusstes
Auftreten wird von Personalern gerne gesehen. Wenn Du Sie überraschst und deine eigene
Persönlichkeit einfließen lässt, ist dir ihre Neugier sicher. Wir erinnern uns an die Fragen aus
der Vorbereitung: Warum interessiert dich der Job? Wie war dein Weg zur Stelle?
Die Antworten dienen dir als Inspiration für den Einstieg in den Text.  
Wenn es an deine Argumente geht, gibt es eine Faustregel:

- **Klar formulieren, nicht schwafeln**

Wer viel redet aber im Endeffekt nichts sagt, kann auch nicht hoffen, gehört zu werden.
Formulierst Du hingegen klar und deutlich, was es wichtiges über dich zu wissen gibt,
bekommt der Personaler schnell ein Bild von dir. Darüber hinaus zeigst Du, dass Du
komplexe Sachverhalte auf das Wesentliche reduzieren kannst. Dieser Ansatz ist
generell hilfreich, um deine Stärken zu beschreiben.  
Deine Fähigkeiten gehen schon mehr oder weniger aus deinem Lebenslauf hervor. Im
Bewerbungsanschreiben kannst Du das jetzt mit Beispielen untermauern. Wie bist Du
bisher mit Schwierigkeiten umgegangen? Welche Erfolge konntest Du verbuchen?
So wird auch der Grundgedanke deines Anliegens deutlich. Du bist nämlich kein Bittsteller,
der um eine Stelle bittet. Vielmehr bietest Du dem Personaler Lösungen an. Aus dieser
Position des Partners ergibt sich auch ein gewisses Selbstvertrauen. Sätze im
Konjunktiv schneiden dir deshalb ins eigene Fleisch. *Hätte*, *würde* und *könnte*
lassen dich unsicher und zögerlich erscheinen. Das gilt im Besonderen für den letzten Satz.

![Kleinkind liest ein Buch][korrekturlesen]

Lies dir dein Bewerbungsschreiben unbedingt noch einmal durch, bevor Du es abschickst.
Am besten einmal, wenn Du es gerade geschrieben hast und noch einmal am nächsten
Tag. Viele Fehler entwischen dir, wenn Du am selben Tag den Text nochmal durchgehst.
Frag' auch mal deine Freunde oder jemanden aus der Familie. Fremde Fehler findet man
viel einfacher als seine eigenen. Verschickst Du deine Bewerbung per Mail, ist der Anhang
eine kleine aber folgenschwere Fehlerquelle. Stell sicher, dass Du die Bewerbung auch wirklich
angehängt hast, bevor Du sie abschickst.

Das Bewerbungsschreiben ist deine Chance, dich persönlich darzustellen. Dazu musst
Du dich natürlich selbst kennen. Mit deiner **Mission**, deinen **Stärken** und deinem **Potenzial**
vor Augen findest Du auch die passende Stelle für dich. Mit einer rhetorisch optimierten
Struktur und den kleinen sprachlichen Feinheiten bringst Du auch eindrucksvoll rüber,
warum Du zur Stelle passt. Und wenn Personaler erkennen, dass Du ihre Probleme löst, bleibt
ihnen nur noch eine mögliche Schlussfolgerung. Dein Bewerbungsschreiben macht so
einwandfrei deutlich, dass Du eingestellt werden solltest.

[Spiegel]: https://static.enwork.de/blog/bewerbungsschreiben-kandidaten/191-100-high-bewerbungsschreiben-sich-erkennen.jpg

[Aufbau]: https://static.enwork.de/blog/bewerbungsschreiben-kandidaten/191-100-high-bewerbungsschreiben-aufbau-anschreiben.jpg

[Schritte]: https://static.enwork.de/blog/bewerbungsschreiben-kandidaten/191-100-high-bewerbungsschreiben-5-schritt-struktur.jpg
[Lupe]: https://static.enwork.de/blog/bewerbungsschreiben-kandidaten/191-100-high-bewerbungsschreiben-formulierung-finden.jpg
[Schlüssel]: https://static.enwork.de/blog/bewerbungsschreiben-kandidaten/191-100-high-bewerbungsschreiben-schluessel.jpg
[korrekturlesen]: https://static.enwork.de/blog/bewerbungsschreiben-kandidaten/191-100-high-bewerbungsschreiben-korrekturlesen.jpg

[Kompetenzen]: https://www.enwork.de/blog/lebenslauf-kandidaten/subsections/kompetenzen/

[Soft Skills]: https://www.enwork.de/blog/lebenslauf-kandidaten/subsections/kompetenzen/

[Udacity]: https://www.udacity.com/courses/career

[Lebenslauf]: https://www.enwork.de/blog/lebenslauf-kandidaten/grundlagen-lebenslauf/