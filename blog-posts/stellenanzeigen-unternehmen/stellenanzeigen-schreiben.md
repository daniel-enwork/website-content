Sie sind der erste Schritt in den Bewerbungsprozess, doch wie schreibt man die
perfekte Stellenanzeige? Diese Frage sorgt schnell für Kopfzerbrechen. Es gilt,
auf wenig Platz viel Inhalt unterzubringen. Und weil die Konkurrenz nicht schläft,
am besten so, dass sich die Anzeige von der Masse abhebt und talentierte
Jobsuchende überzeugt.
Es gibt also eine ganze Menge zu beachten – aber auch eine ganze Menge zu gewinnen.

### Keine zweite Chance für den ersten Eindruck
Die Stellenanzeige ist für Jobsuchende oft der erste Kontakt mit deinem Unternehmen.
Der erste Eindruck zählt und Du hast einige Ansatzpunkte, ihn möglichst ansprechend
zu gestalten. Die zwei Kernaspekte sind eine übersichtliche Struktur und wie
verständlich die Anzeige formuliert ist. Beides sorgt dafür, dass deine Anzeige
auffällt und das Interesse von fähigen Talenten weckt. Gehst Du dann noch individuell
auf deine Wunschbewerber ein, wandelst Du ihr Interesse zum **Bewerbungswunsch**. Dabei
kannst Du die gängigsten Fallen umgehen, wenn Du dir im Vorfeld ein paar Gedanken
machst. Letztendlich nutzt dir eine auf Hochglanz polierte Stellenanzeige nur wenig,
wenn sie niemand findet. Ein paar Kniffe für mehr Reichweite helfen, dem vorzubeugen.

### Übersichtlichkeit: Bewerber sehen darüber nicht hinweg
Eine gute Struktur bildet das Grundgerüst für eine übersichtliche Stellenanzeige.
Welche Informationen Du in welcher Reihenfolge präsentierst, ist der erste
Grundpfeiler und gibt die grobe Struktur vor. Entsprechend wichtig ist diese
Entscheidung, wir schlagen daher ein bewährtes Prinzip aus dem Marketing
vor. In vier Schritt will das **AIDA-Prinzip** vom ersten aufmerksam Werden
zielsicher zum gewünschten Handeln führen. In diesem Fall wäre das, sich auf die
ausgeschriebene Stelle zu bewerben.  
**AIDA** steht dabei für:
- **A**ttention (*Aufmerksamkeit*)
- **I**nterest (*Interesse*)
- **D**esire (*Wunsch*)
- **A**ction (*Handeln*)

![Schiff im Closeup auf See][AIDA]

**Attention**    
Als erstes sicherst Du dir die *Aufmerksamkeit* der Jobsuchenden. Das geht
zum Beispiel mit einem ansprechenden Bild.  
**Interest**  
Hast Du einmal die Aufmerksamkeit deines Leser, kannst Du weiterarbeiten. Mit dem
ersten Eindruck deiner Anzeige weckst Du das *Interesse*, sich weiter mit der
Stelle zu beschäftigen.  
**Desire**  
Vom Interesse bis zum *Wunsch*, sich bei dir zu bewerben: Hier ist, wo die Magie
passiert. Der Leser möchte wissen, was in der Stelle auf ihn oder sie zukommt und welche
Vorteile dich als Arbeitgeber ausmachen. Das ist der Kern deiner
Argumente, warum dein Unternehmen die richtige Wahl ist.  
**Action**  
Im letzten Schritt möchtest Du die Leser zum *Handeln* bewegen. An diesem
Punkt ist die Arbeit eigentlich schon getan. Jetzt geht es nur noch
darum, den Bewerbungswillen nicht wieder zu verlieren. Dazu ist es ratsam, die
Hürde so niedrig wie möglich zu halten. Wer auf einen Blick sieht, was für die
Bewerbung zu tun ist, wird sie auch eher abschicken.

Diese Schritte bilden den Rahmen für den konkreten Text deiner Stellenausschreibung.
Dieser Text ist die **Brücke** von der Jobsuche hinüber zur Bewerbung bei dir. Für
die feinere Struktur kannst Du dich an **5 W-Fragen** orientieren. Die Antworten
auf jede dieser Fragen haben ihren eigenen Absatz verdient, sie sind die
inhaltlichen Bausteine deiner Stellenanzeige:

![Brücke mit fahrenden Autos][Brücke]

- Wer sind wir als Unternehmen?
- Was erwartet die Bewerber?
- Wen suchen wir?
- Was bieten wir?
- Wie kann man sich bewerben?

Die räumliche Aufteilung dieser Bausteine trägt dazu bei, wie verständlich der
Text ist. Das Auge liest schließlich mit. Niemand freut sich, wenn er sich mit
einer Wall of Text konfrontiert sieht. Besonders dann, wenn man schnell die
wichtigsten Punkte einer Stellenanzeige überfliegen will. Mehr Übersicht bieten
zwei Spalten und eine Mischung aus Liste und überschaubaren Fließtext.  

### Verständlichkeit: versteht sich von selbst
Nun stehen die Grundzüge der Struktur. So wichtig es auch ist, deine Inhalte
übersichtlich zu präsentieren, die Inhalte selbst kommen schnell zu kurz.
Die erste Frage für einen Text ist immer das Ziel. Warum schreibe ich den Text
überhaupt, was will ich erreichen? Deine Antwort geht wahrscheinlich in Richtung
"Ich will den richtigen Arbeiter für meine offene Stelle finden". So weit so abstrakt.
Je praktischer Du an dieses Ziel herangehst, desto einfacher wird es dir fallen,
den passenden Text zu schreiben. Was für eine Stelle möchtest Du besetzen? Was bringt
der richtige Arbeiter mit? Ist er ein Student, hat sie viel Arbeitserfahrung?
Wenn Du dir Fragen dieser Art im Vorhinein beantwortest, gestaltest Du eine
sogenannte *Persona*. Diese Persona steht für alle Bewerber, die für deine Stelle
in Frage kommen. Jetzt bleibt noch offen, wie Du diesen idealen Kandidaten abholst.
Vom Titel deiner Stellenausschreibung zur inhaltlichen Aufbereitung hängt alles
von diesen Fragen ab.  
Indem Du deinen Text sprachlich aufpolierst, holst Du die Leser nicht nur ab, sondern
vermittelst auch die wichtigsten Informationen über deine Stelle. Das heißt zunächst
einmal keine unnötig komplizierten Sätze. Keine Fachbegriffe und Fremdwörter, wo es
auch ein verständliches Wort tut. **Kompetenz** und Glaubwürdigkeit zeigen sich nicht
in schlauen Formulierungen, sondern in **sinnvollen Inhalten**.  
Text allein ist aber nicht alles. Bilder in Maßen oder gar Videos an der rechten Stelle
ermöglichen einen handfesten Einblick in die Anstellung und dein Unternehmen.
Bewerber verstehen so eher, worum es geht und können abschätzen, ob sie sich
in deinem Unternehmen sehen oder nicht. Wenn die Leser verstehen, was Du erwartest
und was sie erwarten sollten, schlägst Du dich mit weniger unpassenden Bewerbungen
herum.

### Fallstricke sind ein Fall für sich
![zusammengerolltes Seil][Seil]

Für den letzten Feinschliff lohnt sich ein Blick auf die gängigsten Fragen und
Fallstricke bei Stellenanzeigen. Es gibt einige Fragen, die Du für dich selbst klären
musst und solche, bei denen Du keinen Spielraum hast. Ob Du zum Beispiel schon in
der Anzeige Leser über das Gehalt der Stelle informieren möchtest, liegt bei dir.
Das Gleiche gilt für die Entscheidung zwischen *Sie* und *Du*. Dabei gibt es kein
pauschales Richtig oder Falsch. Entscheidungen dieser Art wirken sich vielmehr
in einer bestimmten Weise aus. Sei dir also im Klaren darüber, was dein **Ziel**
ist und welche **Auswirkungen** dich dem näher bringen. Führst Du nun etwa in der
Stellenausschreibung das *Du* ein, wird es im Bewerbungsgespräch unangenehm für
alle Beteiligten, wenn Du auf das *Sie* bestehst.  
Handfestere Konsequenzen haben Entscheidungen, die sich nicht nach deiner
persönlichen Einschätzung, sondern nach deutschem Recht richten. Namentlich dem
allgemeinen Gleichbehandlungsgesetz **AGG**. Wer sich hier vertut, zahlt schon mal
bis zu 3 Monatsgehälter Entschädigungsanspruch. Das **AGG** soll verhindern, dass
verschiedene Personengruppen ausgegrenzt werden. Die Liste reicht dabei von
Geschlecht über ethnische Herkunft bis zum Alter, entsprechend wachsam lohnt es
sich also zu sein.  
Als Beispiel: Die Formulierung *Deutsch als Muttersprache* impliziert
eine deutsche Herkunft oder zumindest deutsche Eltern als Voraussetzung für die
ausgeschriebene Stelle. Schreibst Du nun aber *sehr gute Deutschkenntnisse*, stellst
Du effektiv die gleiche Anforderung, schließt aber keine talentierten Bewerber mit
Deutsch als Zweit- oder Drittsprache aus. So verhinderst Du juristische Schwierigkeiten
und gleichzeitig gehen dir keine attraktiven Arbeitnehmer durch die Lappen.  

### Reichweite wird reich belohnt
Letztlich bringt dir das beste Konzept nichts, wenn niemand
dein Jobangebot je zu Gesicht bekommt. Das Stichwort hier ist Reichweite. Wie viele
Leser Du erreichst ergibt sich aus verschiedenen Aspekten, angefangen bei dem Titel
deiner Ausschreibung. Hier hast Du zum ersten mal die Chance, auf deine Stelle
aufmerksam zu machen. Jobsuchende scannen nach Anstellungsarten, der Branche und dem
Aufgabenbereich. Dabei ist die Ausdrucksweise ausschlaggebend, kryptische Bezeichnungen
klingen vielleicht professionell, helfen bei der Suche nach dem passenden Job aber
nicht weiter. Denn auch in Sachen Reichweite gilt, dass Du dich nach deinen Bewerbern
richtest. Welche Ausdrucksweise verstehen sie? Auf welchen Jobportalen sind deine
idealen Bewerber unterwegs? Diese Portale sind deine erste Anlaufstelle. Generell
erreicht deine Stellenausschreibung mehr Bewerber, wenn sie nicht nur auf deiner
eigenen Karriereseite zu finden ist, sondern auch auf Jobbörsen wie
[enwork][], LinkedIn oder Xing.  
Wir helfen dir dabei, mit möglichst wenig Aufwand maximalen Effekt zu erzielen.

### Stellenanzeigen sind eine Chance
Dieser Überblick macht deutlich, welche Möglichkeiten eine Stellenanzeige hat.
Du kannst an all diesen Stellschrauben drehen, um genau die Bewerber zu erreichen,
die dir vorschweben. Mit Struktur und Verständlichkeit gewinnst Du sie für dein
Unternehmen. Smarte Talente verschwenden ihre Zeit nämlich nicht mit verkomplizierten
Stellenausschreibungen. Vielleicht findest Du auch noch etwas über deine Unternehmenskultur
heraus und wie Du dein Unternehmen weiterentwickeln kannst. Es lohnt sich in jedem
Fall, dir ein paar Gedanken über dich als Arbeitgeber und die idealen Mitarbeiter zu machen.


[AIDA]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/191-100-high-stellenanzeigen-schreiben-AIDA.jpg
[Brücke]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/191-100-high-stellenanzeigen-schreiben-bruecke.jpg
[Seil]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/191-100-high-stellenanzeigen-schreiben-Seil.jpg

[enwork]: https://www.enwork.de/product-company