Da hast Du deine Stellenanzeige auf Hochglanz poliert, bewusst eine übersichtliche Struktur
gewählt und deinen Text verständlich formuliert – und dann bekommt kein einziges
einsames Talent auf Jobsuche dein Meisterwerk auf den Bildschrim.
Warum? Weil deine Ausschreibung in den Weiten des Internets untergeht. Für den
letzten Feinschliff deiner Stellenanzeige feilst Du am besten an der Reichweite.

### Bevor es losgeht
![Mann vor Tafel][Tafel]

Wie beim Formulieren deiner Texte geht es bei der Reichweite darum, wen Du erreichen willst.
Dabei hilft es, dir zu überlegen, wen Du dir für deine Stelle vorstellst. Was macht
deine idealen Kandidaten aus? Sind sie ausgelernt oder an der Uni?
Bringen sie Fachkompetenzen mit oder sind sie neu in der Branche? Indem Du dich
so in die Jobsuchenden hineinversetzt, erstellst du eine sogenannte *Persona*. Mit
dieser Technik arbeiten auch Marketing Experten, um ihre Zielgruppe zu erreichen.
Die *Persona* ist auch für dich die Grundlage dafür, wie Du Talente im Netz erreichst
und abholst.  
Ob deine Wunschbewerber ihre Suche auf Google beginnen oder sich direkt auf Jobportalen
umschauen, sie arbeiten immer mit Schlagworten über ihre angestrebte Stelle. Für
den Titel deiner Anzeige ist es also günstig, wenn sich die Suchbegriffe deiner
Kandidaten in ihm widerspiegeln. Stell dir deine Stellenanzeige als Antwort auf eine
Anfrage bei Google vor. Wonach suchen Bewerber also in der Regel? Die meisten interessieren
sich für die Art der Anstellung, die Berufsbezeichnung, die Branche und in welcher
Stadt die Stelle liegt.  

### Wohin mit der Stellenanzeige?
Google bietet eine gute Möglichkeit, weiträumig nach Bewerbern zu fahnden und mit
*google for jobs* eine eigene Art von Jobbörse, aber dazu [später mehr](#gjobs).
Möchtest Du aber gezielt auf deine *Persona* zugehen, führt dich der Weg unweigerlich
zu den Jobportale dieser Welt. Diese Plattformen versammeln eine mehr oder weniger
ähnliche Gruppe von Menschen auf ihren Webseiten. So treffen sie eine Vorauswahl
für dich und Du hast eine höhere Chancen, die passenden Bewerber zu erreichen.
Suchst Du etwa in erster Linie Studierende, versprechen die Jobbörsen von Universitäten und
Portale wie Jobmensa oder Campusjäger Erfolg.  
Bei uns auf [enwork][] findest Du Talente aus allen Bereichen.  
Es lohnt sich, deine Stelle auf breit aufgestellten Plattformen wie auch Xing oder LinkedIn
auszuschreiben. Du erreichst nämlich mehr Bewerber, wenn Du auf mehreren Kanälen
gleichzeitig Präsenz zeigst. Für eine höhere Reichweite drängt es sich förmlich auf,
nicht nur die eigene Karriereseite, sonder auch Plattformen wie Xing LinkedIn oder Stepstone
zu nutzen. Deswegen wollen wir dich in Zukunft bei diesem *Multiposting* [unterstützen][].
Schon jetzt bieten wir den Kandidaten auf unserer Webseite an, sich auf Jobs zu bewerben
die sie in den Weiten des Internets entdeckt haben.

### Bewerber am Handy
![Handy auf weißem Grund][Handy]

Mittlerweile gehört es zum guten Ton, sich über die Jugend am Handy aufzuregen.
Damit ist der erste Schritt für mehr Reichweite getan: Die Erkenntnis darüber, dass
sich immer mehr auf dem Smartphone abspielt. So nutzen auch viele Talente verstärkt ihr
Handy, um ihren Traumjob zu finden. Damit sie ihren Traum in deiner Stelle erfüllt sehen,
kannst Du ihnen entgegenkommen. Das sagenumwobene *responsive Design* (zu Deutsch
*reagierendes Design*) macht's möglich.
Einfach gesagt sorgt *responsive Design* dafür, dass sich deine Stellenanzeige an
das Gerät des Nutzers anpasst. Das ist von Vorteil, da die verschiedenen Endgeräte
auch verschiedene Voraussetzungen mit sich bringen.  
Der Bildschirm eines Computers ist klassischerweise breiter als er hoch ist, während
ein Handybildschirm hochkant daherkommt. Dadurch ist am PC an den Seiten deutlich
mehr Platz. Wenn Du diesen Raum voll ausnutzt, wird es auf dem Handy sehr eng.
Deine Anzeige ist dann sehr unangenehm zu lesen, die sogenannte *User Experience* leidet.
Und wenn ein Leser leidet, ist er in der Regel nicht mehr lange ein Leser – zumindest
nicht mehr deiner Stellenanzeige. *Responsive Design* nimmt den Bewerbern diese Last
und lässt sie länger in deiner Anzeige verweilen, sodass sie ihre volle Wirkung
entfalten kann.


<a name="gjobs"></a>
### Google for jobs
![Laptop mit google Startseite][Google]

Im Mai 2019 hat Google seinen Hut in den Ring der Jobplattformen geworfen – und
das ohne eine eigene Plattform im engen Sinne einzurichten. *Google for jobs* **erweitert**
die bekannte Funktion der Suchmaschine um eine Zusammenfassung von Stellenanzeigen
aus dem gesamten Netz. Google durchkämmt dazu Jobbörsen und Karriereseiten wie deine.
Sucht nun jemand etwa nach *Marketing Manager Job* oder *Marketing Manager
Frankfurt*, bekommt er oder sie von Google eine Box mit den Ergebnissen vorgesetzt. Das Ganze
sieht der Nutzer noch bevor die Ergebnisse der normalen Suche, auch *organische Suche*
genannt, angezeigt werden. Mit einem Klick auf die Box öffnet sich eine Ansicht mit allen
Stellen, die Google gefunden hat. Auch die klassischen Filteroptionen anderer Jobportale
haben in diese Ansicht Einzug gehalten. Für die Bewerbung werden die Nutzer auf die Seite
weitergeleitet, auf der Google die Anzeige entdeckt hat.  

Das birgt Potenzial für deine Stellenanzeige, wenn Du aber nicht mitziehst büßt
sie sichtbarkeit im Netz ein. Du kannst deine Stellenanzeige nämlich nicht proaktiv
in diese Vorauswahl einstellen, wie es auf Jobplattformen gängig ist. Stattdessen
gibt Google einige Richtlinien für die Struktur deiner Anzeige vor, damit sie
dargestellt wird.

#### Die Vor- und Nachteile
Der größte **Vorteil** von *google for jobs* liegt in der Übersicht für die
Jobsuchenden. Die Nutzer müssen sich nicht durch die Ergebnisse der organischen
Suche klicken und verschiedene Jobplattformen abklappern. Google analysiert die
verwandte Suchanfragen und spielt deine Anzeige auch, selbst die Schlagworte nicht
eins zu eins übereinstimmen. So erreichst du noch mehr Bewerber, die Box mit Stellenanzeigen
ist als Boost für deine Reichweite viel Wert. Auch wenn sie nicht der Weisheit letzter
Schluss ist.  
**Problematisch** ist, dass Google viele Daten von dir verlangt. Du zahlst zwar
nichts für die extra Reichweite, investierst aber mehr Arbeit in deine Ausschreibung.
Die Bewerbung über google for jobs ist darüber hinaus immer noch kompliziert und
umständlich. Die Kandidaten sind darauf angewiesen, dass die Stellenanzeigen verständlich
geschrieben sind und ihre Nerven nicht zu sehr strapazieren.  

#### Die Vorgaben
![Kompass auf Hand][Kompass]

Google setzt auf Uniformität der Stellenanzeigen. Dabei gibt es zwingende Anforderungen,
ohne die deine Ausschreibung nicht angezeigt wird, und solche, die sie für die
Suchmaschine relevanter macht. Viele dieser Angaben sind aber ohnehin sinnvoll für
deine Stellenanzeige. Alle Vorgaben findest du auf der Seite schema.org,
auch wenn die Seite nicht sehr übersichtlich daherkommt.  
Zwingende Anforderungen:
- datePosted (Gib an, wann die Anzeige veröffentlicht wurde)
- description (Die Stellenausschreibung muss auf HTML basieren)
- hiringOrganization (Gib dein Unternehmen an)
- jobLocation (Gib den Arbeitsort an)
- title (Deine Anzeige braucht einen Stellentitel)
- validThrough (gib an, bis wann die Ausschreibung gültig ist)

empfohlene Anforderungen:
- applicantLocationRequirements (Gib an, wenn Home Office ohne andere Telearbeit
  möglich ist)
- baseSalary (Gib das [Gehalt][] an)
- employmentType (gib die Art der Anstellung an)
- identifier (gib eine eindeutige Kennzeichnung deines Unternehmens an)
- jobLocationType (Gib an, wenn es sich ausschließlich um Heimarbeit handelt)

Letztlich hilft es auch, wenn Du deine Karrierewebseite aktuell hältst und veraltete
Stellenanzeigen herausnimmst. Für dich ist auch wichtig, dass Google for jobs ein
eigenes Layout für deine Ausschreibung wählt. Möchtest Du dein Unternehmen über dein
eigenes Design darstellen macht dir google also einen Strich durch die Rechnung.
Um zu testen, wie gut deine Stelleanzeige gefunden wird, kannst Du selbst einmal
nach ihr auf Google suchen.

### Noch ein paar letzte Tipps
Um das letzte bisschen Reichweite aus deiner Stellenanzeige herauszukitzeln, hilft
dir social Media. Sie sind der Inbegriff von Reichweite in der Welt der Jugend.
Wenn Du deine Ausschreibung mit deinen social Media Kanälen verknüpfst, können deine
Leser die ausgeschriebene Stelle teilen, wenn sie vielleicht nicht für sie selbst
aber für Freunde und Bekannte passt.  
Du musst dich aber nicht einzig und allein auf das Internet verlassen. Wenn Du die
Zeit erübrigen kannst, sei auf Jobmessen, Karrieretagen und ähnlichen Events präsent.
auch hier kannst du an deine *Persona* zurückdenken. Wo wirst du deine Wunschberwerber
wohl antreffen, was sind ihre Anlaufstellen? Je mehr Du auf die Bewerber von dir aus
zugehst, desto eher wirst du deine Stelle ideal besetzen. Ob nun im Netz oder auf
Jobmessen.


[enwork]: https://www.enwork.de/product-company/
[unterstützen]: https://www.enwork.de/product-company/
[Gehalt]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/subsections/fallstricke#Gehalt

[Tafel]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/subsections/reichweite/191-100-high-stellenanzeigen-reichweite-nachdenken.jpg
[Handy]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/subsections/reichweite/191-100-high-stellenanzeigen-reichweite-handy.jpg
[Google]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/subsections/reichweite/191-100-high-stellenanzeigen-reichweite-google.jpg
[Kompass]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/subsections/reichweite/191-100-high-stellenanzeigen-reichweite-vorgaben.jpg