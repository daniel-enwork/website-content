Auf den Jobportalen Deutschlands treiben viele umständlich formulierte Stellenanzeigen
ihr Unwesen, die bei der Jobsuche mehr Fragen aufwerfen als klären. In den meisten
Fällen sorgen Tipps und Tricks zu Übersichtlichkeit und Verständlichkeit
für mehr Klarheit. Neben diesen großen Bereichen gibt es auch noch ein paar unscheinbare
Aspekte, die zwar nebensächlich daherkommen aber weitreichende Folgen haben können.
Diese Fallstricke lassen sich in zwei Kategorien einteilen:  
Entscheidungen nach **deinem persönlichen Stil** und Entscheidungen nach **rechtlichen Grundlagen**.

### Stilentscheidungen nach eigener Vorliebe
Egal wo, egal wann oder wie: Eine Entscheidung hat immer eine gewisse Auswirkung.
Das liegt in der Natur der Sache. Im Falle von Stilentscheidungen gibt es dabei
kein richtig oder falsch. Viel wichtiger ist es, sich den Mechanismus von Ursache
und Wirkung bewusst zu machen. Richtig oder falsch sind erst sinnvoll, wenn Du ein
Ziel vor Augen hast. Die produktiven Fragen an dieser Stelle lauten also:  
Was will ich *erreichen*? Welcher *Effekt* bringt mich meinem Ziel näher? Erzielt
*meine Entscheidung* den gewünschten Effekt?     
Was das konkret bedeutet wird an diesen Fällen deutlich.

<a name="Gehalt"></a>
#### Gehalt angeben?
![Münzen fallen auf Holztisch][Gehalt]

Ein gutes Beispiel ist die **Gehaltsangabe** in der Stellenanzeige. Schon vorwegzunehmen
mit welcher Vergütung die Bewerber rechnen können, ist nicht pauschal richtig oder falsch.
Das heißt aber nicht, dass es egal wäre. Einige Jobsuchende entscheiden auch am Gehalt,
ob die Stelle zu ihnen passt oder nicht – etwa weil sie ihren Lebensstandard halten
möchten oder Familie haben. Bietest du schon in der Stellenanzeige den passenden
Lohn an, gewinnst Du diese Bewerber für dich. Die Gefahr dabei ist, dass Bewerber
abspringen, die von deinem Angebot nicht überzeugt sind. Das muss zunächst einmal
nichts schlechtes sein. So siebst du schon einmal die Bewerber aus, die Du ohnehin
über das Thema Gehalt verlieren würdest. Wenn sie sich gar nicht erst bei dir
bewerben, sparst Du dir also eine Menge Arbeit. Nun soll es aber auch Arbeitssuchende geben,
die gewillt sind, über ihren Lohn zu **verhandeln**. Sofern auch Du dafür offen bist,
kannst Du das deine Leser wissen lassen. Das erreichst Du mit einer Gehaltspanne oder
mit Formulierungen wie *auf Verhandlungsbasis*. So nimmst Du der Gehaltsangabe den
Eindruck von Endgültigkeit und sie wirkt weniger abschreckend.

#### *Sie* oder *Du*?
![Mann hält Pinsel][Wahl]

Eine weitere Stilentscheidung betrifft die **persönliche Ansprache** deiner Leser und
Bewerber. Ob Du das klassische *Sie* oder das respektvolle *Du* bevorzugst, liegt
ganz bei dir. Entspricht es deiner Unternehmenskultur, mit deinen Mitarbeitern
per Du zu sein oder erwartest Du ein Mindestmaß an Respekt und Abstand durch das
höfliche *Sie*? Wie auch immer Du dich entscheidest, bleib' dieser Wahl auch in
der Stellenanzeige treu. Die Stellenausschreibung ist für Bewerber meist der erste
Kontakt mit deinem Unternehmen. Wenn Du hier mit dem *Du* auf mögliche Bewerber
zugehst, werden sie auch im weiteren Bewerbungsprozess vom *Du* ausgehen.
Bestehst Du dann im Bewerbungsgespräch auf das *Sie*, wird dein Gegenüber mit Recht
irritiert sein.  
Wir von enwork sprechen uns für das respektvolle *Du* aus. Es wirkt persönlicher
und erweckt das Gefühl von Nähe. Das sind die Grundlagen für einen Dialog auf
Augenhöhe. Vorausgesetzt es ist kein Problen für dich, geduzt zu werden.


### Recht schnell gefährlich
![gelbes Sicherheitsband vor schwarzem Grund][Recht]

Die Wirkung von einigen Entscheidungen geht aber deutlich über einen Stilbruch hinaus.
Und zwar dann, wenn sie geltendes Recht brechen. Dabei können unscheinbare
Anforderungen wie *Muttersprache Deutsch* schon die Grundlagen für eine Klage sein,
an deren Ende Du bis zu drei Monatsgehälter Entschädigung zahlst. Und das an
jeden abgelehnten Bewerber, der klagt. Klingt komisch, macht aber tatsächlich Sinn,
wenn man den Hintergrund kennt. Klagen dieser Art spielen sich nämlich vor dem
Allgemeinen Gleichbehandlungsgesetzes (**AGG**) ab. Das **AGG** soll
unter anderen Bewerber vor Diskriminierung aufgrund etwa ihres Alters oder
Geschlechts schützen. Stellenanzeigen, die auf Personengruppen eingeschränkt sind,
werden dir Probleme bereiten. Wenn Du sie gar nicht erst ansprichst, sondern
offen und neutral formulierst, sparst Du dir den Ärger.  

#### Was sagt das AGG?
Das **AGG** deckt die folgenden Themen ab: Geschlecht, ethnische Herkunft, Religion,
Behinderung, Alter und sexuelle Identität. Wenn Du dich aufgrund einer dieser Punkte
gegen einen Bewerber entscheidest, hat er oder sie Grund zur Klage. Deswegen bietet es
sich an, den gesamten Bewerbungsprozess samt aller Entscheidungen zu dokumentieren.  
Wenn Du diese Themen umschiffst, bist Du auf der sicheren Seite. Nur wenn bestimmte
Kenntnisse für die Stelle unabdingbar sind, gehören sie in deine Stellenanzeige.
Selbst wenn Du es nicht darauf anlegst, können derartige Voraussetzungen ausgrenzend
wirken. Auch wenn Du Bewerbungen nur per Mail entgegen nimmst, schließt Du ungewollt
ältere Bewerber aus, die in der Regel nicht so fit am PC sind.  
Das **AGG** fordert dir also viel Sorgfalt ab, steht in seiner Natur einer attraktiven
Stellenanzeige aber nicht entgegen.

#### Was sagt die IHK?
Die Fallstricke deutscher Rechtsprechung schneiden dir also keinesfalls den Weg
zu passenden Talenten ab. Damit Du unterwegs auch nicht ins Stolpern gerätst, hat
die Internationale Handelskammer **IHK Wiesbaden** den Umgang mit dem Stück Gesetz
[erschöpfend erklärt][]. Diese Tipps gehen im wesentlichen daraus hervor:   
![Mann gestikuliert und redet][gespeichert]  
Zunächst sollte nicht nur **Ungleichbehandlung vermieden** werden, sondern auch der
**Auswahlprozess dokumentiert** und **archiviert** werden. Abgelehnten Bewerbern reichen Hinweise
auf Ungleichbehandlung aus, um den Fall vor Gericht zu bringen. Um dem etwas entgegenzuhalten,
bedarf es handfester Beweise, dass er oder sie nicht wegen einer der im **AGG**
festgeschriebenen Themen abgelehnt wurde. Deswegen ist die Dokumentation zwar lästig
aber dringend erforderlich.  
In der Ausschreibung selbst empfiehlt die IHK **geschlechtsneutral** zu formulieren.
Also lieber die Funktionsbezeichnung einer Stelle wählen als Jobtitel wie *Jurist*
oder *Krankenschwester*. Ansonsten sorgt der Nachsatz *(m/w/d)* im Jobtitel für Klarheit.  
Auch **Altersgrenzen** schränken Bewerber ein. Ob nun ein *junges und dynamisches
Team* ältere Bewerber ausschließt oder *langjährige Berufserfahrung* junge Bewerber
außen vor lässt: Das Alter eines Bewerbers sollte keine Rolle spielen und ist in
Stellenanzeigen problematisch. Wenn Berufserfahrung für die Stelle unbedingt notwendig
ist, schreib' genau, wie viele Jahre an Erfahrung Bewerber mitbringen müssen.  
Genauso wenig ist eine **Behinderung** relevant. Deine Ausschreibung etwa mit
*körperlich uneingeschränkter Leistungsfähigkeit* zu verknüpfen, ist nur in Ausnahmefällen
zulässig. Eignen sich zwei Bewerber gleich gut, kannst du das Talent mit einer Behinderung
aber vorziehen. Dadurch wirkst du nämlich einem bestehenden Nachteil entgegen, so die IHK.  
Die **ethnische Herkunft** von Bewerbern darf auch kein Kriterium bei der Jobvergabe sein.
Anstatt dein Team um einen *türkischen Mitarbeiter* erweitern zu wollen, solltest Du
nach einem *Mitarbeiter mit Erfahrung im Vertrieb im Nahen Osten* suchen. Und das auch nur,
mittlerweile ahnst Du es sicher schon, wenn es wirklich unbedingt notwendig für die Stelle ist.
Die Anforderung *Deutsch als Muttersprache* bricht genauso mit dem **AGG**.
Das Wort *Muttersprache* impliziert nämlich eine deutsche Herkunft und schließt so all
diejenigen Bewerber aus, die einwandfreies Deutsch sprechen, nur eben als ihre
Zweit- oder Drittsprache. Die Formulierung *sehr gute Deutschkenntnisse* hingegen
lässt keinen Raum für Diskriminierung. Es geht dir im Endeffekt ja auch um **Kenntnisse einer Region**
oder **sehr gute Sprachkenntnisse** und gar nicht um die Herkunft deiner Mitarbeiter.  
Auch ein **Foto** und das **Geburtsdatum** dürfen nicht vom Bewerber eingefordert werden,
da Alter, Geschlecht und ethnische Herkunft keine ausschlaggebenden Kriterien bei der Stellenvergabe
sein dürfen. Bestehst du dennoch auf Foto und Geburtsdatum, kann das als Hinweis ausgelegt werden,
Du würdest dich bei der Entscheidung für den Neuzugang auf diese Themen stützen wollen.

### Was sagst Du?
Viele Kleinigkeiten, die gar nicht so klein sind und viele Entscheidungen, die unvermutete
Auswirkungen haben. Wenn Du weißt, was dein Unternehmen ausmacht und was dir selbst
wichtig ist, wirst Du die kleineren Stolpersteine stilsicher überwinden.
Wenn Du dich fragst, **welche Fähigkeiten** du in einem Bewerber suchst anstatt dich
zu fragen, welche Personengruppen klassischerweise diese Fähigkeiten aufweisen,
wirst du dich auch nicht im Sicherheitsnetz des Arbeitnehmerschutzes verfangen.
Mit ein wenig Sorgfalt und ein paar Gedanken mehr werden die Maschen des **AGG**
für dich nicht zu Fallstricken. Und dir gehen keine fähigen Talente durch's Netz.


[erschöpfend erklärt]: https://www.ihk-wiesbaden.de/recht/rechtsberatung/personal/auswirkungen-des-gleichbehandlungsgesetzes-agg-auf-stellenaussc-1255690

[Recht]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/subsections/fallstricke/191-100-high-stellenanzeigen-fallstrick-recht.jpg
[Gehalt]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/subsections/fallstricke/191-100-high-stellenanzeigen-fallstrick-gehalt.jpg
[Wahl]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/subsections/fallstricke/191-100-high-stellenanzeigen-fallstrick-entscheidung.jpg
[gespeichert]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/subsections/fallstricke/sido_selbst.gif