Offene Stellen freuen kein Unternehmen. Eine gut geschriebene Stellenanzeige ist
die erste Maßnahme, um Abhilfe zu schaffen. Sie ist die Brücke vom Jobportal hinüber
in dein Unternehmen. Mit einer übersichtlichen Struktur fährst Du nicht nur mehr
Bewerbungen ein, sondern findest auch die Kandidaten, die zu deiner Stelle passen.

### Erste Kontaktaufnahme
Die Stellenazeige stellt den ersten persönlichen Kontakt zu deinen potentiellen
Mitarbeitern dar. Diese Begegnung sollte die Bewerber nicht schon von vorne herein
abschrecken. Ganz im Gegenteil willst Du den Lesern ins Auge fallen und ihr Interesse
wecken. Das Zauberwort hierfür lautet Übersichtlichkeit. Mit der passenden Struktur
kannst Du den wenigen Platz, den eine Stellenanzeige nun einmal bietet, optimal
nutzen. Das klingt jetzt wie eine Wissenschaft für sich, doch glücklicherweise
kannst Du dich auch als Arbeitgeber auf die Schultern der Marketing- und Werbeforschung
stellen. In diesem Artikel gewinnst Du einen Überblick über verschiedene Konzepte
und Strategien samt Tipps, wie Du sie miteinander verbinden kannst.  

### Am Anfang war AIDA
![Kreuzfahrtschiff abends bei Sidney][AIDA]

Das wohl bewährteste Prinzip in Sachen Marketing und Werbung ist das sogenannte
**AIDA-Prinzip**. Es beschreibt, wie Du die Aufmerksamkeit eines Lesers gewinnst
und ihn anschließend zum Handeln bewegst. In diesem Fall würde ein Mensch auf der
Jobsuche auf deine Stellenanzeige aufmerksam werden und sich bei dir bewerben.
Den Weg dahin beschreibt **AIDA** in vier namensgebenden Schritten:

- **A**ttention (*Aufmerksamkeit*)
- **I**nterest (*Interesse*)
- **D**esire (*Wunsch*)
- **A**ction (*Handeln*)

**Attention**  
Zu Beginn steht die *Aufmerksamkeit* des Jobsuchenden. Wenn niemand deine
Stellenanzeige liest, wird sich auch niemand bei dir bewerben. Deswegen ist es
wichtig, dass Du den Leser abholst, bevor er oder sie sich mit dem Inhalt deiner
Stelle auseinander setzt. Das klingt jetzt dramatischer, als es ist. Dir stehen
nämlich wirkungsvolle Werkzeuge zur Verfügung. Hier ist der Titel deiner
Stellenausschreibung zu nennen. Er ist der erste Anhaltspunkt für jeden Leser und
ermöglicht einen Einblick in die offene Stelle. Dabei gehen verständliche
Beschreibungen immer über Titel, die spannend klingen aber nichts aussagen. An
diesem Punkt möchte der Leser einfach nur wissen, ob er gefunden hat, wonach er
sucht. Lust auf die Stelle machst Du ihm oder ihr später.  
**Interest**  
Im nächsten Schritt geht es ans *Interesse* der Leser. Jetzt, wo
Du sie ins Lesen verwickelt hast, kannst Du die Stelle spannend und interessant
beschreiben. Dabei lässt Du die Stelle am besten selbst sprechen. Kurz gesagt
möchtest Du dem Lesenden das Gefühl vermitteln, auf der richtigen Fährte zu sein.
Dein Unternehmen und deine Stelle können Wünsche und Bedürfnisse erfüllen oder
eben nicht. Wichtig ist dabei, transparent und ehrlich zu bleiben. So reduzierst
Du auch gleich Bewerbungen, die keine Zukunft haben.  
**Desire**  
Aus dem Interesse wird ein *Wunsch*. Namentlich der Wunsch, sich bei dir zu bewerben.
Tatsächlich gehen Interessen und Wünsche oft Hand in Hand und sind schwer
voneinander zu trennen. Entsprechend ähnlich ist das Vorgehen, nur das der Fokus
hier auf dir und deinem Unternehmen liegt. Was macht dich aus, welche Vorteile
unterscheiden dein Unternehmen von anderen in der Branche? Wenn beim interessierten
Leser der Eindruck entsteht, ein attraktives und vor allem passendes Unternehmen
vor sich zu haben, kommt der Bewerbungswunsch von ganz allein.  
**Action**  
Nun fehlt noch eins: Die Bewerbungswilligen sollen aus ihrer Rolle der Leser
herauswachsen und *handeln*. Er oder sie soll aktiv werden und sich bei dir bewerben,
damit Du  schon bald einen Neuzuwachs für dein Team verzeichnen kannst. Der Mensch
vorm Bildschirm möchte sich also schon bewerben. Es liegt nun an dir und diesem
letzten Schritt, ihm oder ihr diesen Wunsch zu gewähren. Schreibe klar und deutlich,
wie man sich bei dir bewerben kann. An wen soll die Bewerbung gehen? Per Mail oder
postalisch? – Wobei Du deutlich mehr Bewerbungen erwarten kannst, wenn Du den
elektronischen Weg nicht ausschlägst.  
Die Faustregel für's Bewerben lautet *je unkomplizierter, desto mehr Bewerber*.

### Die vier P's und Motivation
Soweit zum theoretischen Unterbau. Bis hierhin bietet das **AIDA-Prinzip** ein
wichtiges Grundverständnis, allerdings wenig handfeste Tipps. Für die wenden wir
das, was wir aus **AIDA** gelernt haben, auf zwei weitere psychologische Prinzipien
an. Es bleibt harmlos und verständlich, keine Sorge.  
Zunächst einmal die **vier P's**. Das sind **P**icture, **P**romise, **P**rove
und **P**ush (zu Deutsch *Bild*, *Versprechen*, *Beweisen* und *Anstoßen*).  
**Bilder** sind der Blickfang schlechthin. Sie bieten viele Informationen in kurzer Zeit,
das Auge bleibt an Bildern eher hängen als an blankem Text. Der Leser
sieht, was er erwarten kann und muss es sich nicht mehr zusammenreimen.  
**Versprechen** bedeutet lediglich, dass Du eine Erwartungshaltung im Lesenden schaffst.
Anders formuliert meint versprechen, dass Du mit Worten ein Bild von deiner Stelle
malst, in dem sich der potentielle Bewerber sehen kann.  
**Beweisen** kannst Du in der Stellenanzeige natürlich noch nichts. Die Idee, den
vermittelten Eindruck aus der Ausschreibung zu bestätigen wird aber beim Thema
Bewerbungsgespräch wieder interessant.  
**Anstoßen** kannst Du den Lesenden jedoch allemal. Dahinter verbirgt sich ein
sogenannter *Call to Action*, ein Aufruf, etwas zu tun. Also in diesem Fall der
Aufruf, sich zu bewerben. Wie unter dem Punkt *Action* aus dem **AIDA-Prinzip** schon
deutlich wurde, sorgen niedrige Hürden dafür, dass dein Anstoß auch Erfolg hat.  

![Mann schiebt Heuballen][push]

Menschen sind träge, besonders wenn sie schon viele schlecht geschriebene Stellenanzeigen
hinter sich haben. **Motivierst** Du sie aber mit deiner Ausschreibung, gewinnst Du sie
für dich und dein Unternehmen. Dabei ist wichtig, *was* motiviert und *wie* sich motivierte
Menschen verhalten.  
Motivation entsteht aus *Motiven*. Das sind **Ziele**, **Wünsche** und **Bedürfnisse**, die
jeder hat und die bei jedem unterschiedlich sind. Marketing-Experten würden dafür
eine sogenannte *Persona* definieren. Auf Deutsch heißt das, sie versetzen sich
in die Lage ihrer potentiellen Kunden und fragen sich, was für ihn oder sie wichtig
ist. Wenn Du die *Motive* deiner idealen Kandidaten ansprichst, motivierst Du genau
die Bewerber, mit denen Du deine Stelle besetzen willst.  
Das **Verhalten** von motivierten Jobsuchenden richtet sich nach ihren *Erwartungen*
und wie viel ihr Ziel ihnen *Wert* ist. Wenn diese beiden Punkte steigen, erhöht
sich auch die Motivation eines Bewerbers und er handelt schneller. Das heißt Leser
bewerben sich eher, wenn sie sich viel von der Stelle erhoffen und einen Mehrwert
in ihr sehen.

### Was heißt das für deine Stellenausschreibung?
Die Lehren aus **AIDA** lassen sich mit den **vier P's** und dem Wissen über
**Motivation** verbinden. Miteinander verflochten bilden sie die Struktur für die
ideale Stellenanzeige. Wir haben hier ein paar handfeste Vorschläge, wie Du die
oben beschriebene Theorie in deine Ausschreibung gießen kannst.  
**Aufmerksamkeit** im Leser erregst Du mit einem deutlichen Titel und über ein
passendes Bild. Die Betonung liegt dabei auf *passend*. Es hilft nicht, ein buntes
Bild zu wählen, wenn es zwar hübsch aussieht aber nichts mit der ausgeschriebenen
Stelle zu tun hat.  
**Interesse** weckst Du, indem Du mit deinem Text eine Art *Versprechen* formulierst.
Das heißt, Du gibst eine Aussicht auf die offene Stelle und dein Unternehmen.
Wenn dir in deinem Unternehmen flexible Arbeitszeiten und Freiheit für deine
Angestellten wichtig sind, gehört das in deine Stellenanzeige.   
Der **Wunsch** zum Bewerben entsteht, wenn Du mit deiner Stellenbeschreibung die
persönlichen Ziele und Bedürfnisse des Lesers ansprichst. Das motiviert und macht
einen Leser zu einem Bewerber. Angehende Handwerker lesen zum Beispiel mit Freude,
dass sie sich in der ausgeschriebenen Stelle auf Praxiserfahrung einstellen können.  
Der Bewerber **handelt**, wenn Du ihn noch einmal dazu aufforderst. Dazu zählt auch,
dass Du klar und deutlich beschreibst, wie man sich bei dir bewerben kann.
Am Ende deiner Bewerbung steht also idealerweise nicht nur *Bewirb dich jetzt!*,
sondern es folgen die entsprechende E-mail Adresse, Postanschrift und alles, was dir sonst
noch wichtig ist.

### Noch Fragen?
![weißes Fragezeichen][Fragezeichen]

Letzlich bieten  dir **5 W-Fragen** einen Leitfaden für den Aufbau deiner Stellenanzeige.
Jede Frage ist dabei die Grundlage für einen Abschnitt deines Textes. So antwortest
Du nacheinander auf die Fragen:
- Wer sind wir als Unternehmen?
- Was erwartet die Bewerber?
- Wen suchen wir?
- Was bieten wir?
- Wie kann man sich bewerben?

Wie im Rest des Lebens gilt die gesamte Anzeige hindurch, dass Klarheit über Geschwafel steht.
Das äußert sich auch in der räumlichen Aufteilung deines Textes. Die berüchtigte *Wall of Text*
hat noch den motiviertesten Bewerber verjagt. Leser verweilen im Text, wenn die
wichtigsten Informationen schnell ersichtlich sind. Der Fließtext hat zwar auch seinen
Platz aber nur in Maßen und auch erst dann, wenn der Leser schon interessiert ist.
Wenn kein Interesse besteht, ist der Leser nicht bereit, viele Informationen aufzunehmen.
Im Zweifel passt die Stelle doch nicht und man hat seine Zeit verschwendet, dieses
Risiko gehen die wenigsten Jobsuchenden ein.  
Hier greift das **AIDA-Prinzip**: erst *Aufmerksamkeit* und *Interesse* der Bewerber
wecken und sie dann über ihre persönlichen *Ziele* und *Bedürfnisse* zum Bewerben veranlassen.
Dafür braucht es dann einen ausformulierten Text. Anders lässt sich kein individuelles
Bild erschaffen und ohne das fühlt sich auch kein Bewerber individuell angesprochen.   

Mit dieser Struktur bringst Du Übersichtlichkeit in deine Stellenausschreibung und
der Bewerber weiß, worauf er sich einlässt. Dadurch erreichst Du die Kandidaten,
die wirklich zu deiner Stelle passen. Das heißt nicht nur, dass sich die Bewerber,
die Du in deiner Stellen sehen möchtest, bei dir bewerben. Gleichzeitig wissen auch
die Leser, die eben nicht bei dir glücklich werden, dass sie weitersuchen
müssen. So sparst Du wertvolle Zeit.  
Es lohnt sich, wenn Du nicht einfach drauf los schreibst, sondern dir vorher ein
paar Gedanken machst. Wer ist mein idealer Bewerber und was kann er oder sie von mir erwarten?
Diese Kernfragen bieten dir den rohen Inhalt für deine Ausschreibung.
Du musst nicht die brandneuesten Theorien aus dem Marketing kennen, um dir ein
paar Einsichten aus der Forschung zu Nutze zu machen.  
Es reicht vollkommen aus, wenn Du diese Grundlagen beachtest und deine Stellenanzeige
mit deiner individuellen Note prägst.

[AIDA]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/subsections/uebersichtlichkeit/191-100-high-stellenanzeigen-uebersichtlichkeit-aida.jpg
[push]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/subsections/uebersichtlichkeit/191-100-high-stellenanzeigen-uebersichtlichkeit-heuballen-schieben.jpg
[Fragezeichen]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/subsections/uebersichtlichkeit/191-100-high-stellenanzeigen-uebersichtlichkeit-fragezeichen.jpg