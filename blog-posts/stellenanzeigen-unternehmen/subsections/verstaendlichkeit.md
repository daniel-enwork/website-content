Aktuell sind unzählige Stellenanzeigen im Umlauf, aus denen Bewerber nicht schlau
werden. Unternehmen setzen auf wichtig klingende Formulierungen und die immer
gleichen Floskeln. Dabei könnte es so einfach sein. Jobsuchende möchten natürlich
interessante Stellen finden aber in erster Linie geht es ihnen um eine Stelle, die passt.
Das entscheidende Kriterium ist dabei, wie verständlich die Arbeit beschrieben ist.
Mehr – und vor allem mehr geeignete Bewerber – verspricht ein
ausgewogenes Verhältnis aus handfesten Inhalt und ansehnlichen Schreibstil.  

### Bevor es los geht
Ein guter Text fällt dir natürlich nicht einfach so zu. Das gilt besonders, wenn
Du eine bestimmte Zielgruppe im Kopf hast. Jeder Leser ist anders und nimmt
ein und denselben Text unterschiedlich auf. Deswegen lohnt es sich, im Vorhinein
zu überlegen, wen Du mit deiner Stellenanzeige erreichen möchtest.  
Marketingexperten würden dazu eine sogenannte *Persona* anlegen. Was hochtrabend
daherkommt ist im Grunde ganz einfach. Eine *Persona* fasst die grundlegenden
Bedürfnisse, Wünsche und Ziele einer Personengruppe zusammen. In deinem Fall wären
das die Bewerber, die Du dir für deine offene Stelle erhoffst. Neben der **Motivation**
beschreibt eine *Persona* auch, wie Du die idealen Bewerber erreichst. Auf welchen
Plattformen sind sie unterwegs? Mit welchem **Sprachstil** können sie etwas anfangen?
Welche Fachbegriffe kennen sie und welche eben nicht?  
Es hilft, diese Fragen realistisch zu beantworten. Letztendlich nützt es dir nicht viel,
nach dem ultimativen Bewerber zu fahnden, wenn er schlicht und ergreifend nicht existiert.

Wenn Du herausgefunden hast, was der ideale Bewerber sucht und wie Du ihn oder sie
am besten ansprichst, kommen Du und deine offene Stelle ins Spiel. Inwiefern erfüllst
Du die **Ziele** deiner Bewerber? Wie rahmst Du die Arbeit so, dass sich die Jobsuchenden
in ihr wiedererkennen? Welche deiner Vorzüge als Arbeitgeber sind besonders wichtig?
Die Essenz dieses Abgleichs lässt sich in einer groben Struktur darstellen,
den **5 W-Fragen**:
- Wer sind wir als Unternehmen?
- Was erwartet die Bewerber?
- Wen suchen wir?
- Was bieten wir?
- Wie kann man sich bewerben?  

Diese Leitfragen sorgen für mehr Übersichtlichkeit im Text und machen ihn so leicht
verständlich. Jede Frage ist die Grundlage für einen Absatz. Wie genau sie ausformuliert
werden, schauen wir uns im Folgenden einmal näher an.

### Verständlichkeit in jedem Schritt
![Fußspuren auf dunklem Sand][Fußspuren]

Zu Beginn steht der Titel deiner Stellenazeige. Er ist ja auch das Erste, was
potenzielle Bewerber über deine Stelle lesen. Der erste Eindruck sitzt mit einem
eindeutig gewählten Titel. Das meint zunächst einmal **verständliche** Begriffe, unter
denen auch die Jobsuchenden etwas verstehen. Es nutzt einfach nichts, verheißungsvolle
Ausdrücke oder gar unternehmensinterne Bezeichnungen zu verwenden. Die Aufmerksamkeit
der Leser sicherst Du dir vor allem dann, wenn klar ist, wovon Du sprichst. Wenn der
Titel klasse klingt aber nichts aussagt, halten die Leser vielleicht kurz inne und
scrollen dann verdutzt weiter.  
Auch für Bewerber hat der Tag nur 24 Stunden. Verrätst Du ihnen schon im Titel,
was sie am meisten interessiert, werden sie einen näheren Blick riskieren. Das
heißt, die **Art der Anstellung** (z.B. Praktikum vs. Festanstellung), die **Branche**,
**Berufsbezeichnung** und ob die Stelle **befristet oder unbefristet** ist, haben
einen Platz in dem Titel der Stellenanzeige verdient.

##### Wer sind wir als Unternehmen?
Stell' zunächst einmal dich und dein Unternehmen vor, damit die Bewerber dich kennenlernen.
Hier sind natürlich deine Produkte beziehungsweise Dienstleistungen zu nennen. Wie groß dein
Unternehmen in etwa ist und welche Erfolge Du bereits zu verzeichnen hattest, tragen
zu einem runden Eindruck von der Arbeitsstelle bei. Wenn für den Leser soweit alles
passt, hast Du sein oder ihr Interesse. Betonst Du, was dein Unternehmen besonders
macht, gewinnst Du an diesem Punkt einen neuen Bewerber. Jetzt ist also Zeit für imposante
Begriffe. Was macht dein Unternehmen aus? Welche Werte vertrittst Du, wie ist die
Arbeitsatmosphäre? Hast Du eine Vision, der sich deine Mitarbeiter anschließen können?  
Es bleibt oberste Priorität, das alles verständlich zu beschreiben. Schreib' besser nichts,
als einen Absatz, der deine Leser verwirrt und den sie nicht verstehen.
Bei aller Selbstdarstellung bleibt der Persona-Gedanke im Vordergrund.
Denn auch wenn es um dich geht, möchtest Du deinen Zielbewerbern bieten, was sie suchen.

##### Was erwarten die Bewerber?
Weiter mit der Stellenbeschreibung. Nachdem die Leser einen Eindruck von deinem
Unternehmen haben, zeigst Du ihnen, was in der Stelle auf sie zukommt.
Das ist namentlich das **Aufgabengebiet** des neuen Mitarbeiters und seine oder ihre
Tätigkeit. Was wird der Mensch, den Du einstellst, bei dir tun? Was unterscheidet
deine Stelle von anderen? Aufgaben, die Du für besonders interessant hältst, kannst
Du gerne explizit erwähnen. Diese Beschreibung spricht idealerweise die **Bedürfnisse
und Ziele** deiner Wunschbewerber an. Denn wenn sie sich gut in diesem Arbeitsalltag
sehen können, keimt in ihnen der Bewerbungswunsch.  
Dieser Effekt entwickelt eine besonders starke Wirkung, wenn Du die **Motivation** der
Leser triffst. Deswegen macht es Sinn, die Aufgaben vor den Qualifikationen
zu beschreiben. Die können nämlich erst einmal abschreckend wirken oder den Leser zumindest
skeptisch stimmen. Hat der Leser allerdings schon sein oder ihr Idealbild vor Augen, wiegen
die Anforderungen nicht mehr so schwer.

##### Wen suchen wir?
![schwarzes Fernglas][Fernglas]

Die Bewerber sollen aber auch zur Stelle passen, nicht nur anders herum. Je genauer
Du beschreibst, was Du im Neuzugang für dein Team suchst, desto besser können die
Leser abschätzen, ob sie gemeint sind. Dabei hilft es, klar zu formulieren, um
welche Anforderungen es geht. Was muss der Bewerber unbedingt mitbringen und was
ist optional? Für den Leser ist diese Information Gold wert. Er oder sie erfüllt
gewisse Qualifikationen oder eben nicht. Wenn Du nur vage formulierst, was wirklich
wichtig ist und worauf Du auch verzichten würdest, verlierst Du fähige Bewerber.
Wie oben angedeutet können zu viele Anforderungen dieselbe Wirkung haben. Wenn Du
dich damit auseinandersetzt, welche Erwartungen tatsächlich realistisch sind,
dürfte das kein Problem sein.  
Hinter all dem liegt der Gedanke, dass Bewerber verstehen möchten. Du hättest es
ja auch lieber, im Vorhinein zu wissen, worauf Du dich einlässt. Das gelingt am
besten mit einem in sich stimmigen Bild. Genau das entsteht im Leser, wenn Du
Fähigkeiten forderst, die unmittelbar mit dem Aufgabengebiet der freien Stelle
zusammenhängen. Passt alles zusammen, kommen auch keine Fragen auf. Das hilft den
Lesern wiederum, sich den Arbeitsalltag greifbar vorzustellen. Der Wunsch, sich
zu bewerben, gedeiht so prächtig.

##### Was bieten wir?
Im Grunde bietest Du eine passende Stelle für aufstrebende Talente an. Den Eindruck
darüber hast Du in den vorangegangenen Absätzen vermittelt. Eine Weißheit aus der
Rederhetorik besagt: *Der Anfang prägt, der Schluss haftet*. Damit deine Stellenanzeige
also einen bleibenden Eindruck hinterlässt, gibst Du den Lesern einen Überblick
darüber, was dein Unternehmen besonders macht. Welche Vorteile genießen deine Angestellten?
Das kann von einem günstigen Fitness Programm über die eigene Mensa bis zu flexiblen Arbeitszeiten
gehen. Können Arbeitgeber mit deiner Unterstützung bei ihrer Weiterbildung rechnen?  
Du musst das Rad nicht neu erfinden, dein individuelles Angebot macht den Unterschied.
Diese sogenannten *Benefits* sind ein maßgeblicher Teil deiner Unternehmenskultur.
Wenn Du diese Kultur offen kommunizierst, wirst Du genau die Bewerber erreichen,
die zu deinem Unternehmen passen. Der Bewerbungswunsch erblüht, jetzt, wo dein
persönlicher Touch den Gesamteindruck von der offenen Stelle abrundet.

##### Wie kann man sich bewerben?
Um die Früchte deiner harten Arbeit zu ernten, kannst Du den Bewerbern ein letztes
mal entgegen kommen. Mit einem einfachen *Bewirb dich jetzt!* ist es nicht getan,
wenn Du nicht auch dazu schreibst, wie das geht. Verrate deinen Bewerbern also ruhig
die entsprechenden Kontaktdaten und auf welchem Weg sie sich bewerben können.  

- An **wen** geht die Bewerbung?  
- Wer ist der **Ansprechpartner** im Bewerbungsprozess und wie erreicht man ihn oder sie?  
-An welche **Adresse** wendet man sich, per E-mail oder postalisch? *(kleiner Tipp: am besten für beides offen sein)*

Je einfacher die Bewerbung ist, desto mehr Leser werden sich auch bewerben.

### Der sprachliche Rahmen
![Frau hält Rahmen][Rahmen]

Der rote Faden dieses Artikels, und letzlich auch unserer [Vision bei enwork][],
lautet **Klarheit** und **Verständlichkeit** für alle Beteiligten. Das spiegelt sich auch
im sprachlichen Stil wieder. Im modernen Bewerbungsprozess hat inhaltloses Geschwafel
keinen Raum. Mit überbordend formulierten Jobtiteln und schwadronierenden Beschreibungen
bar jeglicher handfesten Information tust Du niemanden einen Gefallen.  
Genau. Nochmal in klar und verständlich: leeres Gerede bringt dir keinen Vorteil.
Das erstreckt sich über das ganze Feld der Fremd- und Fachwörter. Ja, sie klingen
schlau und wichtig, machen den Text aber weniger verständlich und schrecken sogar ab.
Der Leser fühlt sich so, wie Du gerade. Im besten Falle irritiert über die unnötig
komplizierte Wortwahl, im schlimmsten Falle springt er ab und liest lieber etwas anderes.  
Als Faustregel kannst Du dir merken
- *wenn Du es einfacher sagen kannst, __sag' es einfacher__*.

Bei aller Einfachheit kann dein Text aber interessant bis spannend geschrieben sein.
Wenn Du nicht immer dieselben Worte wählst, hältst Du deine Leser am Text. Auch eine
wechselnde Satzstruktur sorgt für flüssiges Lesen. Schreib auch mal so etwas wie
*am Arbeitsplatz*, *im Büro* oder *im Arbeitsalltag* anstatt immer nur von *der Stelle*
zu reden. Du musst auch nicht jeden Satz mit *In der Stelle wirst Du...* anfangen.  
- *Ein guter Lesefluss entsteht durch __vielseitige Wortwahl__ und __wechselnden Satzbau__.*


Noch eine Idee für den Hinterkopf, vor allem im Deutschen: Achtung bei Häufungen von
Substantivierungen! Klingt nämlich gar nicht mal so gut, wenn jedes zweite Wort auf *-ung* endet.  
Du kannst dir also merken
- *Wörter auf -ung vermeiden, wenn es auch das __Verb__ tut.*

Nun steht dem allerdings eine Erkenntnis aus der Psycholinguistik scheinbar entgegen.
Die besagt, dass Formulierungen wie *für mehr Freiheit* eher hängen bleiben als
*für mehr freie Bürger*. Substantive können also sehr wohl die bessere Wahl sein,
auch Wörter auf *-ung* erzielen manchmal den gewünschten Effekt. Die Lehre daraus ist,
dass es kein allgemeingültiges Geheimrezept gibt und somit auch kein *nie*, *falsch*
oder *richtig*. Wie Du deinen Text schreibst hat schlicht und ergreifend gewisse
Auswirkungen. Es geht also nicht darum, die richtigen Worte zu wählen. Es geht viel
mehr darum, den Effekt zu erzielen, der dir vorschwebt.  
- *Du kannst es nicht richtig machen, Du kannst es aber auf __deine Weise__ machen.*  

### Gestaltung
Verständlichkeit ergibt sich auch daraus, wie ein Text gestaltet ist. Unsere Kollegen
von Campusjäger führen [Eye Tracking Studien][] an, die sich für ein Bild samt
Firmenlogo in der Kopfzeile und einen erkennbar hervorgehobenen Jobtitel aussprechen.
Bilder sind schön und gut, sie müssen aber thematisch zur Stellenanzeige passen.
Zu viele Bilder kehren ihre Wirkung ins Negative und stören den Leser. Sie sind
also kein Allheilmittel, sondern vorwiegend als Blickfang zu verstehen.  
Dieser Blick möchte dann alles Wichtige mit einem mal sehen und verstehen. Deswegen
sind Fließtexte mit Vorsicht zu genießen. Aus den Eye Tracking Studien wird ersichtlich,
dass ein Block in **Listenform** auf der linken Seite dem Leser entgegenkommt. Interesse
entsteht, wenn er oder sie hier alles wichtige über die **5 W-Fragen** findet. Dafür
hast Du dir ja Gedanken über die Ziele und Bedürfnisse deines Wunschbewerbes gemacht.  
Im rechten Block folgt dann der Fließtext, in dem du näher auf dein Unternehmen und
die Stelle eingehst. Hier stellst Du dann deine sprachlichen Fähigkeiten
unter Beweis und der Bewerbungswunsch des Lesers nimmt seinen Lauf.
Auch im Fließtext kannst Du die wichtigen Stellen **fettgedruckt** hervorheben, das
macht es dem Leser einfacher.  
Mehr Tipps zur Struktur und weiteren Aspekten deiner Stellenanzeige
haben wir in dieser Übersicht besprochen.  

### Es liegt in deinen Händen
![offene Handflächen][Hände]

Bewerber haben eine faire Chance verdient, den passenden Job für sich
zu finden. Es gehören aber immer zwei dazu. Das heißt, dass Unternehmen dieselbe
Chance verdient haben, passende Mitarbeiter zu finden. Als Arbeitgeber bist Du in
der glücklichen Position, es selbst in der Hand zu haben. Mit einer verständlichen
Stellenanzeige erhöst Du deine Chancen und wenn Du ein paar Gedanken investierst,
erreichst Du auch die passenden Bewerber. Vielleicht lernst Du dabei auch noch etwas
über dich selbst und dein Unternehmen.

[Fußspuren]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/subsections/verstaendlichkeit/191-100-high-stellenanzeigen-verstaendlichkeit-titelbild.jpg
[Fernglas]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/subsections/verstaendlichkeit/191-100-high-stellenanzeigen-verstaendlichkeit-fernglas.jpg
[Rahmen]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/subsections/verstaendlichkeit/191-100-high-stellenanzeigen-verstaendlichkeit-rahmen.jpg
[Hände]: https://static.enwork.de/blog/stellenanzeigen-unternehmen/subsections/verstaendlichkeit/191-100-high-stellenanzeigen-verstaendlichkeit-haende.jpg

[Vision bei enwork]: https://www.enwork.de/about-us
[Eye Tracking Studien]: https://arbeitgeber.campusjaeger.de/hr-blog/stellenanzeige-schreiben#bilder