So begibt es sich nun, dass niemand mehr nach draußen geht. Wozu auch, es hat
ja alles zu. Und das ist auch gut so. Das normale Leben legt eine Pause ein,
wir halten zusammen den Atem an. Und zusammenhalten müssen wir, um die Corona Krise
durchzustehen. Damit auch kleine und mittelständische Unternehmen durchhalten, wollen
wir von enwork unseren Beitrag leisten. Als Experten für moderne Bewerbungsprozesse
bereiten wir in diesem Beitrag das Thema **Recruiting im Home Office** auf.  

Es bedürfte eigentlich keines nationalen Notstandes, um sich mit Bewerbungen und
Recruiting auseinanderzusetzen. Die Suche nach neuem Personal ist in aller Regel
ein beschwerlicher Weg. Laut Studien brechen **60 Prozent aller Bewerber** ihre Bewerbung ab.
Und das nicht von ungefähr. Schlechte Kommunikation und umständliche Prozesse sind
feste Bestandtteile der deutschen Bewerbungserfahrung. Darüber hinaus ist vielen
Unternehmen nicht bewusst, dass sie sich mit E-Mail Bewerbungen auf **rechtliches
Glatteis** begeben. Es gilt also, ein paar Kernfragen zu klären.  
Beim [Schreiben der Stellenanzeige][] fängt es ja schon
an – und wohin mit der Anzeige, wenn sie dann steht? Die **Verwaltung** der eingehenden
Bewerbungen stellt Unternehmen vor Herausforderungen, selbst wenn die Personalabteilung
zusammen in einem Büro sitzt. Dazu kommt die Aufgabe, **passende Bewerber zu erkennen**,
ohne ihnen am Tisch gegenüber zu sitzen. Doch so düster muss es nicht sein.
Wie so oft birgt dieser Härtetest auch die Gelegenheit, neue Wege zu gehen.  
Schauen wir uns den Bewerbungsprozess einmal Schritt für Schritt an.

1. Stell' die Grundlage für **kollaborative Arbeit** sicher. Bei der Personalsuche sind
verschiedene Mitarbeiter beteiligt, die gemeinsam planen und Entscheidungen treffen.

2. Schreib' deine **Stelle** so aus, dass deine Wunschkandidaten sie **verstehen**. Kollegen
aus der entsprechenden Abteilung können hilfreichen Input liefern.

3. Veröffentliche deine Stellenanzeige auf **verschiedenen Jobplattformen**. Überleg dir
vorher, wo deine Wunschkandidaten nach Jobs suchen.

4. Kümmer' dich um ein **Verwaltungssystem** für eingehende Bewerbungen. Mit Klarheit in
deinen Prozessen klappt auch die Kommunikation und kein Bewerber geht unter.

5. Mach' dir ein **eigenes Bild** von deinen Bewerbern. Mit deinem persönlichen
Eindruck kannst Du besser einschätzen, wer wirklich in dein Unternehmen passt.

### Das Fundament für gemeinsame Arbeit
![Arbeiter gießen Betonplatte][Fundament]

Die Grundlage für eine gute Zusammenarbeit liegt in transparenten Prozessen. Als
erste Maßnahme gilt es, jeden Mitarbeiter ins Boot zu holen, der im Recruiting seine
Finger im Spiel hat. Alle Beteiligten sollten jederzeit wissen, welche Aufgaben
anstehen, was geschafft ist und wo die Reise überhaupt hingehen soll. Kaum etwas
schneidet der **Effizienz** tiefer ins eigene Fleisch, als eine Hand, die nicht
weiß, was die andere tut.  
<a name="tools"></a>
Glücklicherweise gibt es einige Mittel, um auch online gemeinsam an einem Projekt
zu arbeiten. Hierfür kommen Anwendungen – neudeutsch auch *Tools* genannt – in Frage,
die es den Mitarbeitern ermöglichen, auf ein und dieselben Dokumente und Pläne zuzugreifen.
Als Internet-Unternehmen greifen wir von _**enwork**_ seither auf das *Tool* **Trello** zurück,
ein intuitives Planungsinstrument. Jeder Mitarbeiter kann seine eigenen Aufgaben verwalten
und gleichzeitig sehen, woran seine oder ihre Kollegen gerade arbeiten.  
Um Dokumente gemeinsam zu bearbeiten, bietet sich **Google Docs** an. Auch die
Sammlung **Microsoft Office 365** ist hilfreich. Word und Excel sind weitläufig bekannt,
aber vor allem die Anwendung **OneNote** bringt einen Mehrwert ins Home Office. Mit diesem
*Tool* lassen sich gemeinsam Ideen sammeln und kreativ ausarbeiten. Letztlich
kann jede **Cloud** genutzt werden, wie z.B. der **Google Drive**, um alle möglichen
Dateien zu teilen. Am Ende des Tages kommt es darauf an, dass jeder Mitarbeiter
auf die Dokumente zugreifen kann, die für ihn oder sie wichtig sind.
Wir sind der Meinung, dass Mitarbeiter gemeinsam als Team den gesamten Bewerbungsprozess
einsehen und bearbeiten sollten. Jeder kann sein Wissen beitragen, die technische
Umsetzung muss dem nicht im Wege stehen.

### Die Stelle als Team ausschreiben
![Haende liegen aufeinander][Teamwork]

Es ist eine Kunst für sich. Bei der Komposition einer Stellenanzeige gießt der
geübte Personaler [verständliche][] Worte in eine [übersichtliche][] Struktur und
hat die ganze Zeit über seinen Wunschkandidaten im Hinterkopf. In einer idealen Welt
lesen Jobsuchende so nur wenige Anzeigen, bis sie eine Stelle finden, die zu ihnen passt.
Unternehmen erhalten im Umkehrschluss überwiegend geeignete Bewerbungen und alle sind
glücklich. Die Zeiten sind gerade traurig genug, also belassen wir es an dieser Stelle
bei diesem Idealbild.  
Welche _**Tools**_ also zur Hand nehmen, um ein solches Kunstwerk anzufertigen? Prinzipiell
kann eine Stellenanzeige wie [oben](#tools) dargestellt gemeinsam geschrieben werden.
So kann etwa die Software Entwicklerin ihr Fachwissen in die Anzeige fließen lassen
und der Personalbeauftragte direkt die Feinheiten abstimmen.
Das **weltweite Netz** hält aber auch einige helfende Hände bereit. Anbieter wie
[Personio][] warten mit den entsprechenden *Tools* auf und auch wir von [enwork][]
verstehen uns auf Stellenanzeigen.  
Wir streben dem oben gezeichneten Idealbild entgegen. Einfache Bewerbungen münden
in vielen und vor allem passenden Bewerbungen. Dewegen werden **Verständlichkeit**
und **Übersichtlichkeit** auf unserer Plattform groß geschrieben. Wenn Du dich auf
das Wesentliche konzentrierst, kommt die Einfachheit von ganz alleine.

### Präsenz auf dem Arbeitsmarkt
Die Stellenanzeige kommt frisch aus der Feder. Doch wie geht's jetzt weiter? Wenn
Du eine eigene Karriereseite hast, ist sie hier gut aufgehoben. Es wäre aber schade
um all die Kunstfertigkeit, würde deine Anzeige nur auf dieser einen einsamen Webseite
ihr Dasein fristen. Ob Du nun eine eigene Karriereseite hast oder nicht, es lohnt
sich, auf **verschiedenen Jobportalen** präsent zu sein. Das sogenannte _**Multiposting**_
bildet das zentrale Mittel, um deine Bewerberzahlen zu erhöhen.  
Nun kannst Du selbst die verschiedenen Plattformen händisch mit deiner Anzeige
versorgen oder Du suchst dir Anbieter, die dir die Arbeit abnehmen. Ein Beispiel
ist das *Tool* der Kollegen von [die Personaler][].  

![brauner Briefmschlag liegt auf Tisch][sourcing]

Du kannst aber auch selbst aktiv werden und auf die Arbeitnehmer dieses Landes
zugehen. Das ganze nennt sich _**Active Sourcing**_. Der Vorteil dabei ist, dass Du
Kandidaten an Land ziehst, die sich gar nicht nach einer neuen Anstellung umsehen.
Heutzutage findest Du die meisten Menschen ohnehin nicht mehr draußen, sondern in
diversen **Social Media Kanälen**. Deine ersten Anlaufstellen sind im Bewerbungskontext
also Netzwerke wie [LinkedIn][] oder [Xing][]. Dort kannst Du dir die Profile
verschiedenster Kandidaten ansehen und anschreiben, wer dir geeignet erscheint.  
Nach unserer Vision möchten wir _**Active Sourcing**_ intuitiver gestalten. Auf **enwork** gleichen wir
die offenen Stellen bei uns mit unseren Kandidaten ab und errechnen einen *Matching
Score*. So geben wir dir unsere Einschätzung, welche Kandidaten eine gute Wahl abgeben.
Du kannst natürlich selbst nochmal einen Blick auf die Kandidaten werfen. Wenn dir gefällt,
was Du siehst, kannst Du das dem Jobsuchenden mit einem Klick wissen lassen. Wenn
Du magst auch mit einer persönlichen Nachricht von dir.

### Gebündelte Prozesse für mehr Klarheit
In Deutschland müssen wir natürlich auch über Verwaltung sprechen. Bisher ging
es vornehmlich darum, eine möglichst große Auswahl aus möglichst geeigneten Kandidaten
zu beschaffen. Wenn das gelingt, steht dir eine Flut an Bewerbungen ins Haus. Da gehen
einige Kandidaten auch schon mal unter – wäre schade, wenn das genau deine Wunschkandidaten
trifft. Dieses Phänomen betrifft leider nicht nur die Arbeitsweise im Home Office,
sondern ist auch ohne Krise für viele Unternehmen die größte Schwierigkeit bei der
Mitarbeitersuche.  
Nun gibt es dankbarerweise Software, die dieses Problem löst. Anwendungen dieser
Art nennen sich *Applicant Tracking System* (**ATS**), also Systeme, die die Reise
deiner Bewerber bei dir nachzeichnen. Ein möglicher Anbieter wäre zum Beispiel [greenhouse][].
Je nach Ausführung sammelt ein *ATS* etwa **Informationen** über deine Bewerber, hilft
bei der **Planung** von Prozessen oder erleichtert die **Kommunikation** mit deinen Kandiaten.
So behältst Du die Übersicht darüber, welche Kandidaten schon lange auf eine
Antwort warten oder welche Jobboards die meisten Bewerbungen einfahren. Mit mehr Klarheit
ist der Weg zur Einstellung deutlich produktiver und weniger fehleranfällig.
Wie umfangreich ein *ATS* deine Prozesse verwaltet, hängt vom jeweiligen System ab.
Das gleiche gilt dafür, wie aufwendig Du eine solche Software einrichten musst.  
Wir von [enwork][] denken, dass der gesamten Prozess vom Schreiben der Stellenausschreibung
bis zur Vertragsunterschrift an einem Ort gebündelt verwaltet werden sollte. Das Ganze
soll dann auch ohne großen Aufwand schnell und verlässlich an den Start gehen. So
bleibt mehr Zeit für die Auswahl deiner neuen Mitarbeiter.

### Mach dir ein Bild vom Kandidaten – am besten 30 die Sekunde
![Gruppe sitzt mit Unterlagen vor Laptop][videocall]  

Soweit steht also auch die Verwaltung. An einem Punkt kommst Du aber dennoch nicht
vorbei, wenn Du neues Personal einstellen möchtest: dem **Bewerbungsgespräch**.
Eine **Videokonferenz** macht möglich, was zur Zeit nicht denkbar ist. Die Kontaktbeschränkung
berührt nämlich selbstredend auch den ersten **persönlichen Kontakt** zu deinen Bewerbern.  
Dieser erste Eindruck ist aber Gold wert und Du musst auch nicht auf ihn verzichten.
Ganz im Gegenteil kannst Du dir und deinen Bewerbern mit dem Interview per Video ermöglichen,
was sonst mit Stress und terminlichem Aufwand verbunden wäre.

Whatsapp und Skype sind wohl die bekannstesten *Tools* für Ferngespräche per Bewegtbild.
Sie haben bei aller Einfachheit aber einen Haken: ihre **fragliche Datensicherheit**.
Ganz so einfach ist es nämlich nicht. Jedenfalls nicht mehr seit Mitte 2018, als die
Datenschutzgrundverodnung, kurz **DSGVO**, in Kraft getreten ist. Seither wird die
Datensicherheit in Europa stärker geprüft und kaum einer kennt sich mehr so recht aus.
Die beiden Online-Dienste jedenfalls sind nicht mit europäischem Recht vereinbar,
wenn es um **Datenschutz im Arbeitskontext** geht. Die Daten von Skype und Whatsapp
werden auf amerikanischen Servern gespeichert, also jenseits deines Einflussbereiches.  
Wenn Du nun nachweisen musst, dass alle sensiblen Daten deiner
Kandidaten gelöscht wurden, wird's brenzlig. Selbst bei deinem normalen
E-Mail Verkehr kann es sehr schwierig werden zu beweisen, dass eingegangene Bewerbungen auch
wirklich von jedem Computer gelöscht wurden. Vielen Unternehmen – und vielen Ratgebern
im Netz – ist nicht bewusst, wie genau man es hierzulande mit der Datensicherheit nimmt.
Die Überaschung kannst Du dir auch sparen.

Sprich in jedem Fall mit deinem **Datenschutzbeauftragten**, bevor Du eine Lösung
für deine Online-Interviews zur Hand nimmst. Zeichnest Du das Gespräch auf, musst
Du deine Bewerber aber in jedem Fall darüber informieren und um seine oder ihre
Erlaubnis bitten. Dabei ist ausschlaggebend, dass sie auch eine echte
Wahl haben und sich frei entscheiden können.

### Gemeinsam neue Wege gehen
![drei Leute wandern][neue Wege]

Wie in jeder Krise geht es in diesen Wochen darum, auf **einschneidende Veränderungen**
zu reagieren. Erst einmal scheint nichts mehr sicher aber wie jede Veränderung birgt
unsere Reaktion auf diesen Härtetest eine **Chance**. Das Home Office ist nicht
die optimale Lösung für jeden Angestellten und ganz sicher nicht für alle Aufgaben.
Doch viele Arbeitsabläufe sind schlicht nicht optimal angelegt - weder im Home Office noch im Büro.
Die Notwendigkeit für digitalisierte Bewerbungsverfahren besteht schon länger als
unsere derzeitige Kontaktsperre.
Uns wird lediglich mit Macht vor Augen geführt, was einige sicherlich auch
schon vorher am eigenen Leib gespürt haben. Neues Personal zu finden, kostet zu viel
Zeit und zu viele Nerven. Heutige Prozesse sind **nicht zeitgemäß**.

Über die Hälfte aller Bewerbungen finden deswegen nie ihren Weg zum Wunscharbeitgeber.
Durch langwierige Prozesse verlierst Du talentierte Kandidaten, die Du eigentlich
schon von dir überzeugt hast. Prozesse, die eventuell ein **vermeidbares Risiko**
im Bereich Datenschutz darstellen – ohne dass es dir bewusst ist.  
Doch das muss nicht so sein. Wir haben jetzt gemeinsam die Chance,
**neue Wege** zu gehen. Es geht nicht um eine kurzfristige Lösung für die Zeit während
der Prüfungen dieser Pandemie. Es geht darum, langfristig etwas zu verändern. Darum,
die eigenen Prozesse zu vereinfachen, um sich selbst und seinen
Bewerbern einen Gefallen zu tun.  

Neue Mitarbeiter findest Du im Team. Sorg' also dafür, dass deine Abteilung gut
zusammenarbeiten kann. Gerade die Stellenanzeige profitiert vom Fachwissen deiner
Mitarbeiter. Auf den passenden Jobplattformen erreicht deine Anzeige auch deine
Wunschkandidaten. Oder aber Du gehst proaktiv auf sie zu. Mit klarer Kommunikation und zuverlässigen
Prozessen bewahrst Du das Interesse deiner Bewerber. So wirst Du im persönlichen
Gespräch genau die Talente entdecken, die Du dir für dein Unternehmen wünschst.

![Lady lehnt lässig an Scheinwerfer][Scheinwerfer]

Wir von enwork helfen Unternehmen, sich besser aufzustellen. Dazu
richten wir den Scheinwerfer auf **Recruiting und seine Möglichkeiten**.
Stellenanzeigen können die Kultur deines Unternehmens widerspiegeln und die
Bewerber erreichen, für die sie gemeint sind. Verwaltungsaufgaben können einfach und zentral
vom gesamten Team abgewickelt werden. Und Du kannst immer im Kontakt mit deinen Kandidaten
stehen, um ihnen den Weg an deine Seite zu erleichtern.  

Unsere Webseite stellt [unsere Interpretation][] von modernen Bewerbungsprozessen dar
und bietet einen Rahmen für die Suche nach dem idealen Zuwachs für dein Unternehmen.
Wer in der Krise den rechten Samen sät, wird reich ernten, wenn die Sonne scheint.  
In diesem Sinne, erfolgreiches Rekrutieren und bleib' gesund,

dein Team enwork.


[Schreiben der Stellenanzeige]: https://www.enwork.de/company-blog/stellenanzeigen-unternehmen/stellenanzeigen-schreiben/
[verständliche]: https://www.enwork.de/company-blog/stellenanzeigen-unternehmen/subsections/verstaendlichkeit/
[übersichtliche]: https://www.enwork.de/company-blog/stellenanzeigen-unternehmen/subsections/uebersichtlichkeit/
[Personio]: https://www.personio.de/
[unserer Webseite]: https://www.enwork.de/product-company/
[enwork]: https://www.enwork.de/product-company/
[die Personaler]: https://www.die-personaler.de/html/microsite-multi-posting.html
[LinkedIn]: https://www.linkedin.com/
[Xing]: https://www.xing.com/companies
[greenhouse]: https://www.greenhouse.io/
[unsere Interpretation]: https://www.enwork.de/about-us/

[videocall]: https://static.enwork.de/blog/bewerbungsprozess-unternehmen/subsections/recruiting-im-home-office/191-100-high-bewerbungsprozess-home-office-videocall.jpg
[Fundament]: https://static.enwork.de/blog/bewerbungsprozess-unternehmen/subsections/recruiting-im-home-office/191-100-high-bewerbungsprozess-home-office-fundament.jpg
[Teamwork]: https://static.enwork.de/blog/bewerbungsprozess-unternehmen/subsections/recruiting-im-home-office/191-100-high-bewerbungsprozess-home-office-teamwork.jpg
[sourcing]: https://static.enwork.de/blog/bewerbungsprozess-unternehmen/subsections/recruiting-im-home-office/191-100-high-bewerbungsprozess-home-office-sourcing.jpg
[neue Wege]: https://static.enwork.de/blog/bewerbungsprozess-unternehmen/subsections/recruiting-im-home-office/191-100-high-bewerbungsprozess-home-office-neue-wege.jpg
[Scheinwerfer]: https://static.enwork.de/blog/bewerbungsprozess-unternehmen/subsections/recruiting-im-home-office/191-100-high-bewerbungsprozess-home-office-scheinwerfer.jpg