Dein Lebenslauf ist Grundlage deiner Bewerbung – sozusagen deine Visitenkarte,
mit der Du dich bei einem Unternehmen vorstellst. Einen guten ersten Eindruck
macht diese Visitenkarte vor allem dann, wenn sie für den Personaler alle
wichtigen Infos wie Berufserfahrung und Kompetenzen bereit hält.  
Weniger Arbeit für den Personaler bedeutet bessere Chancen für deine Bewerbung.

### Was macht einen guten Lebenslauf aus?
![Mann in Anzug hält Visitenkarte][Visitenkarte]

Ähnlich einer Visitenkarte kannst Du deinen Lebenslauf kurz, prägnant und
möglichst überzeugend gestalten. Gib ihm eine übersichtliche Struktur.
Der Personaler sieht so mit wenigen Blicken, ob Du für ihn interessant bist.
Wenn Du dann noch individuell auf die offene Stelle eingehst, hast Du gute
Chancen, in die engere Auswahl zu kommen. Das richtige Layout ist mit Word und
Co schnell gemacht. Damit Du auch nichts vergisst, kannst Du zum Beispiel bei
[Xing](https://lebenslauf.com/ratgeber/checkliste) oder
[Stepstone](https://www.stepstone.de/Karriere-Bewerbungstipps/lebenslauf/#53)
nach Checklisten und Tipps für den Lebenslauf suchen.

### Was kommt in meinen Lebenslauf
![schwarz weiß Tastatur][Tastatur]

Allgemein fasst Du deine bisherige Berufserfahrung zusammen und gibst die
Eckdaten deiner schulischen Ausbildung sowie deine persönlichen Daten an.
Das Ganze rundest Du mit passenden Praktika, Ehrenämtern, Hobbys und Interessen
ab. Es kann sich lohnen, dich mit einem Foto von dir zu präsentieren.
Der Personaler kann sich so leichter vorstellen, wie Du auf der Arbeit wirkst.  
Dein Lebenslauf ist die erste Anlaufstelle für einen Personaler,
dich einzuschätzen. Das heißt konkret, er sucht nach **Anhaltspunkten**, ob deine
Fähigkeiten und auch Du als Person zu der ausgeschriebenen Stelle passen.
Dein Lebenslauf ist deine Gelegenheit, ihm eine Zusammenfassung all deiner
Argumente dafür zu liefern.

### Was kann ich überhaupt?  
![Walnussschale][Nussschale]

Zwischen Begriffen wie "Projekt- und Implementierungserfahrung",
"EDV-Kenntnissen" und "ABC-Analyse" kann einem schon mal der Kopf rauchen.
Es ist nicht immer leicht zu erkennen, welche Fähigkeiten sich hinter den
Anforderungen an eine Stelle verbergen. Und allzu oft steht man alleine da,
wenn es darum geht, seine **Fachkompetenzen** anzugeben. Im Grunde müsste es aber
gar nicht so kompliziert sein:  
Deine Fachkompetenzen sind schlicht und ergreifend **überprüfbare** Fähigkeiten,
die Du durch **Berufserfahrung** oder **Workshops** erlernen kannst. Dazu zählen
der Umgang mit MS Office, Erfahrung im EDV-Bereich, CAD-Kenntnisse, Programmiersprachen
wie Java oder auch Sprachkenntnisse.  

### Was macht einen Skill soft?
![Walnusskerne][Nusskerne]

Neben den **Fachkompetenzen** wirst Du auch schon eine Reihe von
**Sozialkompetenzen** aufweisen können. Die brauchst Du nämlich nicht nur im Job,
sondern auch im täglichen Leben. Kurz gesagt sind es diejenigen Fähigkeiten, die
den Umgang mit deinen Mitmenschen prägen. Da sie schwer überprüfbar sind,
spricht man auch von **Soft Skills**. Ob Du bei Diskussionen sehr überzeugend
wirken kannst, oft gesagt bekommst, dass Du ein guter Zuhörer bist oder deine
Truppe auch in schwierigen Situationen gut zusammenhältst: Diese
**zwischenmenschlichen Merkmale** machen deine Sozialkompetenzen aus.
Ein Personaler wird in deinem Lebenslauf nach Hinweisen auf diese Art von
Fähigkeiten Ausschau halten.

### Was spricht für Sprachkenntnisse?  
![Mann steht in Bücherei und liest][Bücherei]

Letztendlich können wir nicht über deinen Lebenslauf sprechen, ohne über deine
**Sprachkompetenzen** zu reden. Denn je nach Branche wird spätestens im
Bewerbungsgespräch genau dieses Thema zur Sprache kommen. Wie Du zu deinen
Sprachkenntnissen gekommen bist, kannst Du im Lebenslauf mit einem
Auslandssemester, Sprachreisen, oder einem Gap Year eindrucksvoll darstellen.  
Es ist aber manchmal schwer einzuschätzen, wie gut man eine Sprache tatsächlich
spricht.

### Was kann mein Lebenslauf?
Deinen Lebenslauf anzulegen, ist weitaus mehr als lästiger Papierkram.
Hier liegt eine Menge Potenzial für deine Bewerbung. Du kannst dem Personaler
zeigen, mit wem er es zu tun hat und warum Du genau die richtige Wahl für die
Position bist. Während Du so deinen Lebenslauf ausgestaltest, lernst Du vielleicht
auch noch etwas über dich selbst. Du bekommst mehr als nur eine neue Visitenkarte
für deine Bewerbungen. Und wenn Du die Stelle dann bekommst, kannst Du sicher sein,
dass sie zu dir passt.

[Visitenkarte]: https://static.enwork.de/blog/lebenslauf-kandidaten/191-100-high-grundlagen-lebenslauf-visitenkarte.jpg
[Tastatur]:  https://static.enwork.de/blog/lebenslauf-kandidaten/191-100-high-grundlagen-lebenslauf-laptop-tastatur.jpg
[Nussschale]: https://static.enwork.de/blog/lebenslauf-kandidaten/191-100-high-grundlagen-lebenslauf-fachkompetenzen.jpg
[Nusskerne]: https://static.enwork.de/blog/lebenslauf-kandidaten/191-100-high-grundlagen-lebenslauf-sozialkompetenzen.jpg
[Bücherei]: https://static.enwork.de/blog/lebenslauf-kandidaten/191-100-high-grundlagen-lebenslauf-sprachen.jpg