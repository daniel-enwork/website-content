Das Foto im Lebenslauf ist keine Pflicht. Der Personaler muss sich aber kein
Bild mehr von dir machen, wenn Du schon eins für ihn bereit stellst. So bleibst
Du ihm im Gedächtnis und das macht deine Bewerbung attraktiv – aber nur wenn Du
ein Bild in guter Qualität auswählst.

### Bild' dir deine Meinung
Ob Du deiner Bewerbung ein Profilbild beilegen willst, liegt ganz bei dir.
Mit dem allgemeinen Gleichbehandlungsgesetzt (**AGG**) sichert dir der Staat dein
Recht auf eine Bewerbung ohne Foto von dir zu. Das soll Diskriminierung
vorbeugen, kann dir aber auch einen entscheidenden Vorteil nehmen.
Viele Personaler würden sich nämlich über etwas greifbare Persönlichkeit in
deinem Lebenslauf freuen. Mit deiner Austrahlung kannst Du noch mehr rüberbringen
als mit den kurzen Abrissen über die Stationen in deinem Lebenslauf.
Ein neues Gesicht bleibt eher hängen als der tausendste Absatz über den immer
gleichen Abschluss. So kann der Personaler schon mal ungefähr abschätzen, wie Du
am Arbeitsplatz wirken wirst. Wird in der Stellenausschreibung aber explizit
um eine Bewerbung ohne Bild gebeten, ist es sinnvoll, dieser Bitte nachzukommen.
Am Ende des Tages soll der Personaler ja mit deiner Bewerbung zufrieden sein.

### Dein Profilbild für den ersten Eindruck
![mann mit Bart Profilbild][Mann]

Wenn Du dich mit einem Foto von dir bewirbst, kannst Du dem Personaler zeigen,
wer Du bist. Mit deinem Bild kannst Du untermauern, was Du dann später
schreibst, nämlich dass Du zur Stelle passt. Deswegen ist es sinnvoll, dir zu überlegen,
welche **Fähigkeiten** und **Kompetenzen** der Personaler in dir sucht.
Willst Du zum Beispiel als Webdesigner durchstarten, macht es Sinn, dich
aufgeschlossen und kreativ zu zeigen. Soll es der Job als Sales-Spezialist
werden, sind eine seriöse Ausstrahlung und Zielstrebigkeit angemessen.

### Stellschrauben für ein gutes Bild
![stehende Schraube scharf und liegende Schrauben unscharf][Schrauben]

Den gewünschten Effekt erreichst Du über **Körpersprache** und **deine Kleidung**.
Du kannst dich darüber informieren, was in der Branche gängig ist.
Als angehender Bankkaufmann ist der Anzug zum Beispiel dem bunten Shirt samt
Goa-Hose vorzuziehen. Du kannst generell darauf achten, dich so zu präsentieren,
wie Du auch zum Bewerbungsgespräch erscheinen möchtest. So beugst Du
Überraschungen auf der Arbeitgeberseite vor.  
Neben deinem Outfit und deiner Körpersprache kannst Du noch an weiteren
Schrauben drehen, um möglichst viel aus deinem Profilbild herauszuholen.
Zunächst hilft ein guter **Kontrast** zwischen dir und dem Hintergrund, dich
hervorzuheben. Ein heller Hintergrund dürfte dabei auch das Gemüt des Personalers
aufhellen, dem dein Bild in die Hände fällt.

### Zeig dich, wie Du bist
Wenn Du dann noch einen frischen und aufgeweckten Eindruck vermittelst, umso
besser. Es lohnt sich wirklich ausgeschlafen und mit guter Laune zum Fototermin
zu kommen. Auf dem Bild sieht man, wenn Du dich nicht wohl fühlst. Auch wenn Du
mit dem Fotografen nicht zurecht kommst, hinterlässt das Spuren. Dein Profilbild
wird dich am besten widerspiegeln, wenn Du entspannt Du selbst sein kannst.
Natürlichkeit ist immer noch das beste Mittel, um sympathisch zu wirken.  

### Probier' dich aus
![Frau mit Stil vor orangener Wand][Frau]

Die Fotosession beim Fachmann ist die ideale Gelegenheit, Neues auszuprobieren.
Mit einem guten Fotografen hast Du nämlich auch gleich einen erfahrenen Berater
an deiner Seite.  
Farben, die Du nicht so häufig trägst, Kombinationen, die Du immer schon mal
ausprobieren wolltest oder das schicke Hemd, das Du eigentlich nur zum Feiern
anziehst: Für die neue Stelle könnten sie genau das Richtige sein. Hol' dir ruhig
eine zweite Meinung von Freunden und Familie ein. Ob das Foto selbst in Farbe
oder in schwarz-weiß sein soll, hängt von dir ab. Auch hier tut es nicht weh,
zu fragen, wie das Bild auf andere wirkt. Spiel' mit den verschiedenen
Möglichkeiten, bis Du wirklich zufrieden bist.

### Bewerben mit Persönlichkeit
Es steckt viel Arbeit und Engagement in einem Profilbild. Durch diesen Aufwand
bekommst Du einen persönlichen Kanal, um einen Eindruck von dir zu vermitteln.
So machst Du deinen Lebenslauf nahbarer und er bleibt im Gedächtnis – wenn Du dich
natürlich zeigst und dich wohl fühlst. Ein gutes Gefühl kannst Du nämlich nur
übertragen, wenn Du es auch selbst hast. Dein Profilbild kann dich also von deiner
besten Seite zeigen und Türen öffnen, die dir sonst verschlossen blieben.

[Mann]:  https://static.enwork.de/blog/lebenslauf-kandidaten/subsections/profilbild/191-100-high-profilbild-lebenslauf-mann-mit-bart.jpg
[Frau]: https://static.enwork.de/blog/lebenslauf-kandidaten/subsections/profilbild/191-100-high-profilbild-lebenslauf-fancy-frau.jpg
[Schrauben]: https://static.enwork.de/blog/lebenslauf-kandidaten/subsections/profilbild/191-100-high-profilbild-lebenslauf-schrauben.jpg