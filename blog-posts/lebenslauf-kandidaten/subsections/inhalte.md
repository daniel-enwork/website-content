Nun haben wir schon alle viel erlebt in unserem Leben. Schule, Studium oder
Ausbildung – ja vielleicht sogar beides – und hier und da noch ein wenig
Berufserfahrung. Um das alles in einen gut lesbaren Lebenslauf zu gießen,
braucht es eine übersichtliche Form und einen Hauch Individualität.

### Dein Lebenslauf in alter Frische
Es gibt einige Sachen, die in deinem Lebenslauf einen festen Platz haben.
Schon aus praktischen Gründen ist es sinnvoll, Eckdaten wie deinen vollen Namen,
deine Anschrift und Kontaktdaten anzugeben. Mit Geburtsdatum und Geburtsort
vervollständigst Du die klassische Kopfzeile und kannst nun anfangen,
deinem Lebenslauf eine individuelle Note zu verleihen. Du kannst herausstechen,
indem Du deine angestrebte Position nennst, bevor Du in das typische Format mit
Ausbildung und Berufserfahrung einsteigst.

### Inhaltlich punkten
![Mann dunkt schwarz weiß][punkten]

Der Kern deines Lebenslaufes ist und bleibt deine berufliche Erfahrung und
schulische Ausbildung. Es hilft, sich bewusst zu machen, warum sich der Personaler
deine Lebensgeschichte überhaupt durchliest. Ihm geht es nämlich in erster Linie
darum, einen Eindruck von deinen **Fähigkeiten** und **persönlichen Talenten**
zu bekommen. So kann er abschätzen, ob Du das mitbringst, was er sich für die
offene Stelle vorstellt. Ein Schulprojekt, bei dem Du die Organisation übernommen
hast oder dein Engagement im Sportverein können etwa für Organisationstalent und
Teamfähigkeit sprechen.  
Nun kannst Du zwar nicht ändern, was Du in deinem Leben gemacht hast aber Du
kannst es der Stelle entsprechend auslegen. So nimmst Du dem Personaler die
Detektivarbeit ab und stellst dich gleichzeitig so dar, wie Du von ihm
wahrgenommen werden möchtest.

### Mit Struktur und rotem Faden
Ein übersichtlicher Lebenslauf legt alles wichtige offen, was es über dich zu
wissen gibt.  
Damit der Personaler auch gleich mit den aktuellsten Informationen in Kontakt
kommt, bietet sich der amerikanische Aufbau an. Das heißt konkret, dass die
neuesten Stationen am Anfang stehen und die älteren dem zeitlichen Ablauf nach
folgen. Wenn dein Schul- oder Uniabschluss das aktuellste Thema sind, kannst Du
auch chronologisch vorgehen. Hauptsache Du wählst einen Stil und bleibst ihm den
ganzen Lebenslauf über treu.

### Schlank und relevant
Dein Lebenslauf lebt von den Stationen, die Du durchlaufen hat. Vor allem, weil
sie in irgendeiner Weise dazu beigetragen haben, dich zu formen. Der Personaler
interessiert sich dabei für die **Fähigkeiten** und **Talente**, die dich
ausmachen. Ein kurzer Stichpunkt, in dem Du erzählst, was Du während einer
Station gelernt hast, hilft dem Personaler, deine Kompetenzen ausfindig zu
machen. Was hast Du in einer Stelle gemacht? Welche Erfolge konntest Du verbuchen?
Bei der schulischen Ausbildung bietet es sich an, relevante Abschlussnoten zu
erwähnen.

### Mit Bezug zum Zug kommen
Es gibt mehr über dich zu erzählen, als nur das Grundgerüst aus Erfahrungen und
Ausbildung. Deine Praktika und Nebenjobs können ein guter Weg sein, um zu
untermauern, was Du kannst und was dich ausmacht. Wenn Du etwa schon zu
Schulzeiten in einem Buchladen gejobbt hast, wird das den Personaler von einem
Verlag interessieren.  
In Richtung Soft Skills ist viel über deine Interessen und Hobbys zu holen.
Wenn Du zum Beispiel in deiner Freizeit zeichnest, kannst Du das in deinem
Lebenslauf für die Stelle als Architekturzeichner gerne erwähnen. Ob es ein
Praktikum oder deine Hobbys sind, wenn Du eine **sinnvolle Verbindung** zur
ausgeschriebenen Stelle herstellst, sind sie in deinem Lebenslauf gut aufgehoben.

### Gutes Layout zahlt sich aus
![Koffer voller Geld][Koffer]

Das Erscheinungsbild deines Lebenslaufes kann dem Personaler das Leben einfacher
machen. Wenn Du nach *CV Layout* oder *Lebenslauf Vorlage* googlest, findest Du
**verschiedenste Varienten**. Word bietet ebenfalls Mustervorlagen, mit denen Du
deinen Lebenslauf auch ohne Probleme selbst gestalten kannst. Klassische Formen
sind tabellarisch aufgebaut, mit den **Stationen** links und den entsprechenden
**Fähigkeiten** und **Kenntnissen** rechts. Alternativ dazu kannst Du eine
Listenform wählen, in der Stichpunkte unter die einzelnen Stationen kommen.
Letztendlich liegt es an dir. Wenn Du dich auf eine kreative Stelle wie
Webdesigner bewirbst, kannst Du deinen Ideenreichtum demonstrieren. Willst Du es
lieber dezent halten, kannst Du wichtige Details mit fettgedruckter Schrift
hervorheben.  
Kurz und übersichtich erfüllt dein Lebenslauf Personaler-Träume.
Mit deiner Unterschrift verleihst Du ihm noch einen persönlichen Touch.

### Dein Lebenslauf, deine persönliche Note
Im Mittelpunkt deines Lebenslaufes stehen Du, dein angestrebter Job und warum
ihr so gut zusammen passt. Wenn Du ihm individuellen Flair einhauchst, kannst Du
dem Personaler genau das vermitteln. Selbst wenn es dir schwerfallen sollte, passende
Fähigkeiten und Lebensstationen zu nennen, hilft dir das im Endeffekt weiter.
Mit deinem Lebenslauf hast nämlich auch Du eine bessere Übersicht darüber, was
Du kannst und welche Stelle zu dir passt.

[punkten]: https://static.enwork.de/blog/lebenslauf-kandidaten/subsections/inhalte/191-100-high-inhalt-lebenslauf-basketballer.jpg
[Koffer]:  https://static.enwork.de/blog/lebenslauf-kandidaten/subsections/inhalte/191-100-high-inhalt-lebenslauf-geldkoffer.jpg