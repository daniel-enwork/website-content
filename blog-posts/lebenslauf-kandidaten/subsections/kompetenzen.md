Fachlich qualifiziert und sozial kompetent, diesen Eindruck will ein Personaler
von deinem Lebenslauf haben. Die Bausteine für diesen Eindruck sind deine
**Hard Skills** und **Soft Skills**. Dabei kannst Du deinen Lebenslauf als Rahmen
für deine Qualifikationen und persönlichen Talente nutzen.

### Zwischen EDV und Teamfähigkeit
Stellenanzeigen bieten eine Vielzahl an Anforderungen und gewünschten Kompetenzen.
Da ist es nicht immer leicht, den Überblick zu behalten. Mit ein wenig
Hintergrundwissen kannst Du dich in diesem teils wirren Dschungel zurechtfinden.
Grundlegend unterteilen sich deine Fähigkeiten in **Fachkompetenzen**, also etwa
Qualifikationen, die Du nachweisen kannst, und **Sozialkompetenzen**, eher
persönlichen Eigenschaften, die dich auszeichnen.  
Viele Blogs und Webseiten sprechen von sogenannten **Hard Skills** bzw. **Soft
Skills**. Dabei meint *Hard Skill* deine Fachkompetenzen und Qualifikationen,
wie etwa Kenntnisse in der elektronischen Datenverarbeitung (EDV) oder eine
abgeschlossene Ausbildung. Fähigkeiten, die in dieses Feld fallen, kannst Du
demonstrieren oder nachweisen. Und genau hier liegt der Unterschied zu den *Soft
Skills*. Die kann dir nämlich kein Stück Papier attestieren, Du kannst sie viel
mehr selbst einschätzen. Ob Du zum Beispiel gut im Team arbeiten kannst, wird
sich schon bei Gruppenarbeiten zu Schulzeiten gezeigt haben.  
Welche fachlichen und sozialen Kompetenzen wichtig sind, ist von Job zu Job
unterschiedlich.

### Fachkompetenzen, deine greifbaren Fähigkeiten
![Hand fährt durch Wasser][Wasser]

Zu deinen Fachkompetenzen zählen also konkretes Wissen und nachweisbare
Fähigkeiten. Eine berfuliche Ausbildung, Workshops an der Uni oder
Weiterbildungsprogramme im Job sind klassische Quellen für diese Fähigkeiten.
Sie sind meist die Voraussetzung für eine Stelle, da sie im Berufsalltag
unumgänglich sind. Durch die regelmäßige Anwendung schärfst Du deine
Fachkompetenzen vor allem im Beruf.  
Genau das sucht ein Personaler in deinem Lebenslauf, wenn er sich die
einzelnen beruflichen Stationen durchliest. Zu deiner
Ingenieursausbildung kannst Du in einem Stichpunkt deine CAD-Kenntnisse anmerken.
Wenn Du einen Workshop zu Führungskompetenz im Zuge einer Stelle absolviert hast,
kann auch das Erwähnung finden. Auch Fremdsprachenkompetenzen sind
Bestandteil deiner Fachkompetenz.

### Handfeste Beispiele für handfestes Wissen
Deine Kompetenzen alleine sind aber nur die halbe Miete. Wenn Du sie im Bezug auf
deine verschiedenen beruflichen Lebensstationen in Szene setzen kannst, steigerst
Du die Aussagekraft deiner **Hard Skills**. Mit einer Notiz im Lebenslauf zu den
Aufgaben, die mit der Kompetenz verbunden waren, ist das schnell gemacht.
Dazu noch eine ausführlichere Beschreibung im Bewerbungsschreiben und
der Personaler kann leichter nachvollziehen, was Du tatsächlich auf dem Kasten hast.

### Auf die Stelle kommt es an
Es bleibt zu beachten, auf welche Stelle Du dich bewirbst. Idealerweise bist Du
mit den Kernkompetenzen deines Fachbereichs vertraut und erkennst sie in der
Stellenausschreibung wieder. Sollten dennoch Unklarheiten auftauchen, kann der
Titel oder die Beschreibung der Stelle meist Abhilfe schaffen. Ansonsten lohnt
sich eine kurze Suche im
[Internet](https://lmgtfy.com/?q=was+ist+%5BGESUCHTE+FACHKOMPETENZ%5D).

### Sozialkompetenzen, deine persönlichen Talente
![Junge mit Strohhut und Geige][Geigenjunge]

Deine Sozialkompetenzen sind eher **charakterliche Eigenschaften**. Sie zeichnen
deine Herangehensweise an die Arbeit aus und wie Du mit deinem Mitmenschen
umgehst. Für den Job sind sie genau so wichtig wie deine Fachkompetenzen. Du
erkennst sie aber vor allem an Erfahrungen, die Du außerhalb des Berufs gemacht
hast.  
Ein gutes Organisationstalent zeigt sich zum Beispiel meist schon im Schulalltag.
Ob Du ein Talent für Zeitmanagement hast, macht sich in der Uni beim Schreiben
von Hausarbeiten bemerkbar. Ein Ehrenamt deutet auf Engagement hin und wenn die
Tätigkeit nicht gerade dein Fachgebiet ist, kannst Du dich getrost als flexibel
einschätzen.  
**Sozialkompetenzen** sind so individuell wie Menschen. Was genau dich ausmacht
kannst Du herausfinden, indem Du dich intensiv mit deinem Lebenslauf
auseinandersetzt.

### Kompetenz ist nicht gleich Kompetenz
Erfahrungsgemäß ist Transparenz nicht die Stärke einer Stellenausschreibung.
Welche Kenntnisse und Talente tatsächlich gemeint sind, ergibt sich oft erst
aus dem **Kontext** der Stelle. Beispielsweise steht hinter der geforderten
Fachkompetenz "SAP-Kenntnisse" nicht immer das gleiche Wissen.   
SAP ist ein Programm, das in der Verwaltung verwendet wird. Durch verschiedene
Zusatzfunktionen kann die Software von Buchführung bis zum Personalwesen in
verschiedensten Bereichen angewendet werden.  
Wer das eine Modul beherrscht kennt sich also nicht zwangsläufig mit einem
anderen aus. Ein Blick in die Stellenbeschreibung sorgt meist für Klarheit,
welche Kompetenz der Personaler eigentlich erwartet. Wenn das immer noch nicht
reicht, kannst Du nachfragen.

### Mehr als nur eine Liste
![leers Blatt mit Stift und Kaffee][Blatt]

Hinter Fach- und Sozialkompetenzen stecken mehr als eine reine Aufzählung von
**Fähigkeiten** und **Talenten**. Sie sind die Merkmale, die dich auf dem Arbeitsmarkt
aber auch im persönlichen Leben auszeichnen. Dein Lebenslauf spricht von dieser
Lebendigkeit.  
Wie genau Du die Inhalte deines Lebenslaufes geschickt präsentierst, ist
also eine Überlegung wert. Deine Erfahrungen prägen dich. Und das zeigt sich darin,
wie Du an Probleme und unbekannte Situationen herangehst. Persönlichkeitstests
sind ein guter erster Schritt, sich in einem Persönlichkeitstyp wiederzufinden
und bietet eine Basis für eigene Gedanken.
Wenn Du weißt, wie Du tickst, findest Du auch leichter den passenden Job für dich.

[Wasser]: https://static.enwork.de/blog/lebenslauf-kandidaten/subsections/kompetenzen/191-100-high-kompetenzen-lebenslauf-wasser.jpg
[Geigenjunge]: https://static.enwork.de/blog/lebenslauf-kandidaten/subsections/kompetenzen/191-100-high-kompetenzen-lebenslauf-junge-spielt-geige.jpg
[Blatt]: https://static.enwork.de/blog/lebenslauf-kandidaten/subsections/kompetenzen/191-100-high-kompetenzen-lebenslauf-leeres-blatt.jpg