Am Anfang war die Sprachkenntnis. Wenn man nicht dieselbe Sprache spricht, wird
es nämlich schwer mit der Kommunikation – das gilt im Urlaub wie im Beruf.
Entscheidend dabei ist, wie gut man mit einer Fremdsprache umgehen kann.

### Sprache ist, wenn man spricht
![zwei Männer im Gespräch][Gespräch]

Englisch bis zum Abi, mehrere Jahre Französisch in der Schule schmerzvoll hinter
sich gebracht und dann steht man da im Urlaub und kriegt kein Wort raus. Wie
lange Du eine Sprache sprichst hat wenig Aussagekraft darüber, wie gut Du sie
letztendlich sprechen kannst. Ein halbes Jahr in England täglich sprechen zu
müssen schult die Sprachkenntnis besser als zwei Jahre lang im Englischunterricht
nicht richtig zuzuhören. Im Beruf ist der sichere Umgang mit Fremdsprachen
teilweise sogar eine Voraussetzung, um überhaupt an den Job zu kommen. Was aber
genau *verhandlungssicheres Englisch* oder eine *gute Kenntnis der Sprache*
ausmacht, ist in der Stellenausschreibung meist der eingenen Interpretation
überlassen.

### Europaweit abgesprochen
Das entscheidende Merkmal der **Sprachbeherrschung** ist das **Niveau**, auf dem
man sich tatsächlich befindet. Um Sprachkenntnisse miteindander vergleichen zu
können, kann der sogenannte "gemeinsame europäische Referenzrahmen für Sprachen"
(**GER**) zu Rate gezogen werden. Als Schüler bekommt man davon in der Regel
zwar nichts mit aber der **GER** ist die Grundlage für Sprachunterricht und
Sprachzertifikate in ganz Europa.

### Strukturiert und niveauvoll
Personaler vertrauen bei der Auswahl ihrer Kandidaten auf genau diesen
Referenzrahmen, da er verlässlich und unkompliziert ist. Es hilft bei der
Jobsuche also, sich mit den verschiedenen **GER**-Niveaus ein wenig auszukennen –
auch um dich selbst angemessen einschätzen zu können.  
Die Struktur ist dankbarerweise simpel aufgebaut:  
Es gibt **drei Hauptniveaus**, A bis C, die in jeweils **zwei Untergruppen**,
1 und 2, aufgeteilt sind. Sprachkenntnisse können entsprechend von **A1**, den
elementaren Grundkenntnissen, bis zu **C2** reichen, was einem nahezu
mutterpsrachlichen Niveau entspricht. Der **GER** ist in erster Linie funktional
konzipiert. Die Stufen richten sich also danach, was der Sprecher einer gewissen
Stufe mit der Sprache tun kann.

### A1-A2: elementare Grundkennisse
Die erste Stufe bei jeder Fremdsprache sind die **Grundlagen**. Im **A1-A2**
Niveau wird die grundlegende Grammatik vermittelt. Man lernt einige Ausdrücke
auswendig, etwa um sich vorzustellen oder zu sagen, wo man wohnt. Der Sprecher
macht noch schwerwiegende Fehler, da das System der Grammatik noch nicht
vollständig verstanden wurde.  
**Auf A2 Niveau** kannst Du dich also zu einfachen Fragen über dich selbst
äußern, machst aber noch viele Fehler und bist darauf angewiesen, dass dein
Gegenüber dir hilft und das Gespräch führt.

### B1-B2: selbstständiges Sprechen
Diese Stufe wird oft als **fortgeschritten** bezeichnet. Grammatik und Satzbau
funktionieren überwiegend gut, das Reden läuft auch schon einigermaßen flüssig
und bei vertrauten Themen kann man auch schon mitreden. Zwar wird man immer mal
wieder nach Worten suchen und sich mit Umschreibungen helfen, aber **der
Sprecher kann sich verständlich machen**  
**Auf B2 Niveau** kannst Du ein Gespräch aktiv mitgestalten, Fehler werden
seltener und meist von dir selbst korrigiert. Du kannst dich klar ausdrücken,
sprichst gleichmäßig und verwendest die ersten komplexen Satzstrukturen.

### C1-C2: kompetentes Sprechen
Auf dem höchsten Sprachniveau angekommen wird die Sprache **sicher beherrscht**.
Der Sprecher macht nahezu keine Fehler mehr und behält ein hohes sprachliches
Niveau bei allen Themen. Mit einem umfangreichen Vokabular kann man sich zu allen
allgemeinen wie auch beruflichen und wissenschaftlichen Themen äußern und seine
Beiträge geschickt in ein Gespräch einflechten.  
**Auf C2 Niveau** erkennst Du auch sprachliche Feinheiten und kannst dich in
verschiedenen Varianten ausdrücken. Komplexe Satzstrukturen sind bei
einwandfreier Grammatik und natürlichem Sprachfluss kein Problem. Du kannst dich
auch umgangssprachlich ausdrücken und kennst die für die Sprache typischen
Redewendungen.

### Sprache im Beruf
![Frau erklärt an Whiteboard][erklären]

Im Beruf ist der sichere Sprachgebrauch meist das Minimum. Vor allem wenn es um
*verhandlungssicheres Englisch* geht, reichen Kenntnisse unter dem **C1-C2**
Niveau nicht aus. Das liegt ganz einfach daran, dass die Sprache an sich auf
diesem Niveau keine besondere Aufmerksamkeit mehr erfordert. Man kann sich auf den
reinen Inhalt des Gesprächs konzentrieren und vor allem auf **C2** Niveau auch die
entscheidenden **sprachlichen Feinheiten** des Gegenübers erkennen und auch selbst
Nuancen setzen.  
Wenn Du noch überlegen musst, wie der Satz jetzt eigentlich richtig ist, wird es
schwer, genau das zu vermitteln, was Du sagen willst. Kenntnisse auf dem **A1-A2**
Niveau sind zwar für den Small-Talk im Urlaub nützlich aber im Beruf seltener
relevant. Der Personaler kann mit dieser Information nur etwas anfangen, wenn die
Sprache thematisch zum Job passt und nicht tatsächlich gesprochen werden muss.

### Mehr als nur Sprechen
Eine Sprache zu beherrschen bedeutet mehr, als ein paar Regeln zu kennen und
Informationen verstehen und verständlich machen zu können. Hinter einer Sprache
steckt nämlich ein kultureller Bezugspunkt. Deswegen reicht es auch kaum aus, im
Unterricht Grammatikregeln zu lernen. Eine Sprache sprechen bedeutet, sie
anzuwenden: mit deinen Mitmenschen zu reden, ein Buch zu lesen, einen Film oder
ein Video zu schauen.  
Dadurch kannst Du gelerntes Wissen an eigene Erfahrungen anknüpfen, Du verbindest
etwas mit der Sprache. Entsprechend ist deine Fremdsprachenkompetenz viel mehr
als eine weitere Qualifikation für die neue Stelle. Wann immer sich die
Gelegenheit bietet, dein Sprachverständnis zu vertiefen, lohnt es sich
zuzuschlagen. Sei es ein halbes Jahr im Ausland oder die neue Serie auf Netflix
auf Englisch mit Untertiteln zu schauen. Eine Sprache wirklich zu sprechen,
öffnet dir Tür und Tor.  
Die Tür in den neuen Job ist eine davon.  

![offene Tür mit Vorhängeschloss][Tür]

[Gespräch]: https://static.enwork.de/blog/lebenslauf-kandidaten/subsections/sprachen/191-100-high-sprachen-lebenslauf-gespraech.jpg
[erklären]: https://static.enwork.de/blog/lebenslauf-kandidaten/subsections/sprachen/191-100-high-sprachen-lebenslauf-frau-redet.jpg
[Tür]: https://static.enwork.de/blog/lebenslauf-kandidaten/subsections/sprachen/191-100-high-sprachen-lebenslauf-offene-tuer.jpg