#!/bin/sh
npm install -g ajv-cli

PASSED=0

for f in $(find $1/blog-posts/ -name "*.json"); do
  echo "Checking $f"
  if ! ajv -s $1/config/blog-post-schema.json -d $f ; then
    PASSED=1
  fi
  echo ""
done

exit $PASSED
