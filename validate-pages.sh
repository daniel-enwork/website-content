#!/bin/sh
npm install -g ajv-cli

PASSED=0

for f in $1/pages/*.json; do
  echo "Checking $f"
  if ! ajv -s $1/config/page-schema.json -d $f ; then
    PASSED=1
  fi
  echo ""
done

exit $PASSED
